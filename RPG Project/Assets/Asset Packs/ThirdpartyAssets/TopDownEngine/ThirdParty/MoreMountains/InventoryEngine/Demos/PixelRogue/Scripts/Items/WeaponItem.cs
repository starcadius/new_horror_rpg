﻿using UnityEngine;
using System.Collections;
using MoreMountains.Tools;
using System;
using RPG.PlayerInventory;
using RPG.Combat;

namespace MoreMountains.InventoryEngine
{	
	[CreateAssetMenu(fileName = "WeaponItem", menuName = "MoreMountains/InventoryEngine/WeaponItem", order = 2)]
	[Serializable]
	/// <summary>
	/// Demo class for a weapon item
	/// </summary>
	public class WeaponItem : InventoryItem 
	{
		
		[Header("Weapon")]
		/// the sprite to use to show the weapon when equipped
		public Sprite WeaponSprite;
		[SerializeField] Weapon weapon = null;
		/// <summary>
		/// What happens when the object is used 
		/// </summary>
		public override bool Equip()
		{
			base.Equip();
			Debug.Log("weapon item equip weapon sprite ="+ WeaponSprite);
			//InventoryDemoGameManager.Instance.Player.SetWeapon(this);
			InventoryDemoGameManager.Instance.Player.GetComponent<Fighter>().EquipWeapon(weapon);
			return true;
		}

		/// <summary>
		/// What happens when the object is used 
		/// </summary>
		public override bool UnEquip()
		{
			base.UnEquip();
			Debug.Log("weapon item unequip weapon sprite =" + WeaponSprite);
			//InventoryDemoGameManager.Instance.Player.SetWeapon(this);
			InventoryDemoGameManager.Instance.Player.GetComponent<Fighter>().UnEquipWeapon();
			return true;
        }
		
	}
}