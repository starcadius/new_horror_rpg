﻿using UnityEngine;
using System.Collections;
using MoreMountains.Tools;

namespace MoreMountains.InventoryEngine
{	

	[RequireComponent(typeof(Rigidbody2D))]
	/// <summary>
	/// Demo character controller, very basic stuff
	/// </summary>
	public class InventoryDemoCharacter1 : MonoBehaviour, MMEventListener<MMInventoryEvent>
	{
		[Information("A very basic demo character controller, that makes the character move around on the xy axis. Here you can change its speed and bind sprites and equipment inventories.",InformationAttribute.InformationType.Info,false)]

		
		/// the sprite used to show the current weapon
	    public SpriteRenderer WeaponSprite;
		/// the armor inventory
		public Inventory ArmorInventory;
		/// the weapon inventory
		public Inventory WeaponInventory;

	    protected int _currentArmor=0;
	    protected int _currentWeapon=0;


		
		/// <summary>
		/// Sets the current armor.
		/// </summary>
		/// <param name="index">Index.</param>
	    public virtual void SetArmor(int index)
	    {
	    	_currentArmor = index;
	    }

		/// <summary>
		/// Sets the current weapon sprite
		/// </summary>
		/// <param name="newSprite">New sprite.</param>
		/// <param name="item">Item.</param>
	    public virtual void SetWeapon(Sprite newSprite, InventoryItem item)
	    {
			WeaponSprite.sprite = newSprite;
	    }

		/// <summary>
		/// Catches MMInventoryEvents and if it's an "inventory loaded" one, equips the first armor and weapon stored in the corresponding inventories
		/// </summary>
		/// <param name="inventoryEvent">Inventory event.</param>
		public virtual void OnMMEvent(MMInventoryEvent inventoryEvent)
		{
			if (inventoryEvent.InventoryEventType == MMInventoryEventType.InventoryLoaded)
			{
				if (inventoryEvent.TargetInventoryName == "RogueArmorInventory")
				{
					if (ArmorInventory != null)
					{
						if (!InventoryItem.IsNull(ArmorInventory.Content [0]))
						{
							ArmorInventory.Content [0].Equip ();	
						}
					}
				}
				if (inventoryEvent.TargetInventoryName == "RogueWeaponInventory")
				{
					if (WeaponInventory != null)
					{
						if (!InventoryItem.IsNull (WeaponInventory.Content [0]))
						{
							WeaponInventory.Content [0].Equip ();
						}
					}
				}
			}
		}

		/// <summary>
		/// On Enable, we start listening to MMInventoryEvents
		/// </summary>
		protected virtual void OnEnable()
		{
			this.MMEventStartListening<MMInventoryEvent>();
		}


		/// <summary>
		/// On Disable, we stop listening to MMInventoryEvents
		/// </summary>
		protected virtual void OnDisable()
		{
			this.MMEventStopListening<MMInventoryEvent>();
		}
	}
}