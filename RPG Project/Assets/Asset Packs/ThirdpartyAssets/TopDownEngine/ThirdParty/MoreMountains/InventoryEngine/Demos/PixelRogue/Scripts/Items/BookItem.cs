﻿using UnityEngine;
using System.Collections;
using MoreMountains.Tools;
using System;

namespace MoreMountains.InventoryEngine
{	
	[CreateAssetMenu(fileName = "BookItem", menuName = "MoreMountains/InventoryEngine/BookItem", order = 2)]
	[Serializable]
	/// <summary>
	/// Demo class for a boook item
	/// </summary>
	public class BookItem : InventoryItem 
	{
		[Header("Health Bonus")]
		/// the amount of health to add to the player when the item is used
		public int HealthBonus;
		[Header("Mana Bonus")]
		/// the amount of Mana to add to the player when the item is used
		public int ManaBonus;
		[Header("Insanity Bonus")]
		/// the amount of insanity to add to the player when the item is used
		public int InsanityBonus;

		/// <summary>
		/// When the boook gets used, we display a debug message just to show it worked

		/// </summary>
		public override bool Use()
		{
			base.Use();
			// This is where you would increase your character's health,
			// with something like : 
			// Player.Life += HealthValue;
			// of course this all depends on your game codebase.
			Debug.LogFormat("increase character's health by " + HealthBonus);
			return true;
		}		
	}
}