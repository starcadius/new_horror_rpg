﻿using UnityEngine;
using System.Collections.Generic;
using MoreMountains.InventoryEngine;

namespace PixelCrushers.DialogueSystem.InventoryEngineSupport
{

    /// <summary>
    /// Adds Lua functions to work with Inventory Engine.
    /// </summary>
    [AddComponentMenu("Pixel Crushers/Dialogue System/Third Party/Inventory Engine/Inventory Engine Lua (on Dialogue Manager)")]
    public class InventoryEngineLua : MonoBehaviour
    {

        public bool debug;

        private Dictionary<string, Inventory> inventoryCache = new Dictionary<string, Inventory>();
        private bool didIRegister = false;

        private static bool s_registered = false;

        void OnEnable()
        {
            if (!s_registered)
            {
                didIRegister = true;
                s_registered = true;
                Lua.RegisterFunction("mmAddItem", this, SymbolExtensions.GetMethodInfo(() => mmAddItem(string.Empty, string.Empty, (double)0)));
                Lua.RegisterFunction("mmRemoveItem", this, SymbolExtensions.GetMethodInfo(() => mmRemoveItem(string.Empty, string.Empty, (double)0)));
                Lua.RegisterFunction("mmGetQuantity", this, SymbolExtensions.GetMethodInfo(() => mmGetQuantity(string.Empty, string.Empty)));
                Lua.RegisterFunction("mmUseItem", this, SymbolExtensions.GetMethodInfo(() => mmUseItem(string.Empty, string.Empty)));
                Lua.RegisterFunction("mmDropItem", this, SymbolExtensions.GetMethodInfo(() => mmDropItem(string.Empty, string.Empty)));
                Lua.RegisterFunction("mmEquipItem", this, SymbolExtensions.GetMethodInfo(() => mmEquipItem(string.Empty, string.Empty)));
                Lua.RegisterFunction("mmUnEquipItem", this, SymbolExtensions.GetMethodInfo(() => mmUnEquipItem(string.Empty, string.Empty)));
                Lua.RegisterFunction("mmEmptyInventory", this, SymbolExtensions.GetMethodInfo(() => mmEmptyInventory(string.Empty)));
            }
        }

        void OnDisable()
        {
            if (didIRegister)
            {
                didIRegister = false;
                s_registered = false;
                Lua.UnregisterFunction("mmAddItem");
                Lua.UnregisterFunction("mmRemoveItem");
                Lua.UnregisterFunction("mmGetQuantity");
                Lua.UnregisterFunction("mmUseItem");
                Lua.UnregisterFunction("mmDropItem");
                Lua.UnregisterFunction("mmEquipItem");
                Lua.UnregisterFunction("mmUnEquipItem");
                Lua.UnregisterFunction("mmEmptyInventory");
            }
        }

        public Inventory FindInventory(string inventoryName)
        {
            if (inventoryCache.ContainsKey(inventoryName) && inventoryCache[inventoryName] != null)
            {
                return inventoryCache[inventoryName];
            }
            var go = GameObject.Find(inventoryName) ?? Tools.GameObjectHardFind(inventoryName);
            var inventory = (go != null) ? go.GetComponent<Inventory>() : null;
            if (inventory != null)
            {
                if (inventoryCache.ContainsKey(inventoryName))
                {
                    inventoryCache[inventoryName] = inventory;
                }
                else
                {
                    inventoryCache.Add(inventoryName, inventory);
                }
            }
            if (inventory == null && Debug.isDebugBuild) Debug.LogWarning("Dialogue System: Can't find Inventory GameObject named '" + inventoryName + "'.", this);
            return inventory;
        }

        public InventoryItem FindItem(string itemID)
        {
            if (string.IsNullOrEmpty(itemID)) return null;
            var item = DialogueManager.LoadAsset(itemID) as InventoryItem;
            if (item == null || !string.Equals(item.ItemID, itemID)) item = DialogueManager.LoadAsset("Items/" + itemID) as InventoryItem;
            if (item == null || !string.Equals(item.ItemID, itemID))
            {
                var itemAssets = Resources.LoadAll<InventoryItem>("");
                for (int i = 0; i < itemAssets.Length; i++)
                {
                    if (string.Equals(itemAssets[i].ItemID, itemID))
                    {
                        item = itemAssets[i];
                        break;
                    }
                }
            }
            if (item == null && Debug.isDebugBuild) Debug.LogWarning("Dialogue System: Can't find item type '" + itemID + "' in a Resources folder or AssetBundle.");
            return item;
        }

        public int FindItemIndex(Inventory inventory, string itemID)
        {
            if (inventory == null || inventory.Content == null) return -1;
            for (int i = 0; i < inventory.Content.Length; i++)
            {
                var content = inventory.Content[i];
                if (content == null) continue;
                if (string.Equals(content.ItemID, itemID)) return i;
            }
            return -1;
        }

        public void mmAddItem(string inventoryName, string itemID, double quantity)
        {
            try
            {
                if (debug) Debug.Log("Dialogue System: mmAddItem('" + inventoryName + "', '" + itemID + "', " + quantity + ")");
                var inventory = FindInventory(inventoryName);
                if (inventory == null) return;
                var itemToAdd = FindItem(itemID);
                if (itemToAdd == null) return;
                inventory.AddItem(itemToAdd, (int)quantity);
            }
            catch (System.Exception e)
            {
                Debug.LogException(e);
                throw e;
            }
        }

        public void mmRemoveItem(string inventoryName, string itemID, double quantity)
        {
            try
            {
                if (debug) Debug.Log("Dialogue System: mmRemoveItem('" + inventoryName + "', '" + itemID + "', " + quantity + ")");
                var inventory = FindInventory(inventoryName);
                if (inventory == null) return;
                if (inventory.Content == null) return;
                var leftToRemove = (int)quantity;
                for (int i = 0; i < inventory.Content.Length; i++)
                {
                    var content = inventory.Content[i];
                    if (content == null) continue;
                    if (string.Equals(content.ItemID, itemID))
                    {
                        var amountToRemoveFromSlot = Mathf.Min(leftToRemove, content.Quantity);
                        leftToRemove -= amountToRemoveFromSlot;
                        inventory.RemoveItem(i, amountToRemoveFromSlot);
                        if (leftToRemove <= 0) return;
                    }
                }
            }
            catch (System.Exception e)
            {
                Debug.LogException(e);
                throw e;
            }
        }

        public double mmGetQuantity(string inventoryName, string itemID)
        {
            try
            {
                var inventory = FindInventory(inventoryName);
                if (inventory == null) return 0;
                var quantity = inventory.GetQuantity(itemID);
                if (debug) Debug.Log("Dialogue System: mmGetQuantity('" + inventoryName + "', '" + itemID + "') returns " + quantity);
                return inventory.GetQuantity(itemID);
            }
            catch (System.Exception e)
            {
                Debug.LogException(e);
                throw e;
            }
        }

        public void mmUseItem(string inventoryName, string itemID)
        {
            try
            {
                if (debug) Debug.Log("Dialogue System: mmUseItem('" + inventoryName + "', '" + itemID + ")");
                var inventory = FindInventory(inventoryName);
                if (inventory == null) return;
                inventory.UseItem(itemID);
            }
            catch (System.Exception e)
            {
                Debug.LogException(e);
                throw e;
            }
        }

        public void mmDropItem(string inventoryName, string itemID)
        {
            mmRemoveItem(inventoryName, itemID, 1);
        }

        public void mmEquipItem(string inventoryName, string itemID)
        {
            try
            {
                if (debug) Debug.Log("Dialogue System: mmEquipItem('" + inventoryName + "', '" + itemID + ")");
                var inventory = FindInventory(inventoryName);
                if (inventory == null) return;
                var item = FindItem(itemID);
                var itemIndex = FindItemIndex(inventory, itemID);
                if (item == null || itemIndex == -1) return;
                inventory.EquipItem(item, itemIndex);
            }
            catch (System.Exception e)
            {
                Debug.LogException(e);
                throw e;
            }
        }

        public void mmUnEquipItem(string inventoryName, string itemID)
        {
            try
            {
                if (debug) Debug.Log("Dialogue System: mmUnEquipItem('" + inventoryName + "', '" + itemID + ")");
                var inventory = FindInventory(inventoryName);
                if (inventory == null) return;
                var item = FindItem(itemID);
                var itemIndex = FindItemIndex(inventory, itemID);
                if (item == null || itemIndex == -1) return;
                inventory.UnEquipItem(item, itemIndex);
            }
            catch (System.Exception e)
            {
                Debug.LogException(e);
                throw e;
            }
        }

        public void mmEmptyInventory(string inventoryName)
        {
            try
            {
                if (debug) Debug.Log("Dialogue System: mmEmptyInventory()");
                var inventory = FindInventory(inventoryName);
                if (inventory == null) return;
                inventory.EmptyInventory();
            }
            catch (System.Exception e)
            {
                Debug.LogException(e);
                throw e;
            }
        }

    }
}