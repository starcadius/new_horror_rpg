using System;
using GameDevTV.Utils;
using RAD.MeleeSystem;
using RPG.Core;
using RPG.Saving;
using RPG.Stats;
using UnityEngine;

namespace RPG.Resources
{
    public class Health : MonoBehaviour, ISaveable, IDamageReceiver
	{
        [SerializeField] float regenerationPercentage = 70;

        LazyValue<float> healthPoints;

        bool isDead = false;

        private void Awake()
        {
			healthPoints = new LazyValue<float>(GetInitialHealth);
           
        }
		private float GetInitialHealth()
		{
			return GetComponent<BaseStats>().GetStat(Stat.Health);
		}

		public void OnEnable()
		{
			GetComponent<BaseStats>().onLevelUp += RegenerateHealth;
		}

		public void OnDisable()
		{
			GetComponent<BaseStats>().onLevelUp -= RegenerateHealth;
		}

		private void Start()
		{
			healthPoints.ForceInit();
		}

		public bool IsDead()
        {
            return isDead;
        }

        public void TakeDamage(GameObject instigator, float damage)
        {
            healthPoints.value = Mathf.Max(healthPoints.value - damage, 0);
            if(healthPoints.value == 0)
            {
                Die();
				if(instigator!=null)
                AwardExperience(instigator);
            }
        }

		public float GetHealthPoints()
		{
			return healthPoints.value;
		}

		public float GetMaxHealthPoints()
		{
			return GetComponent<BaseStats>().GetStat(Stat.Health);
		}

		public float GetPercentage()
        {
            return 100 * (healthPoints.value / GetComponent<BaseStats>().GetStat(Stat.Health));
        }

        private void Die()
        {
            if (isDead) return;

            isDead = true;
            GetComponent<Animator>().SetTrigger("die");
            GetComponent<ActionScheduler>().CancelCurrentAction();
        }

        private void AwardExperience(GameObject instigator)
        {
            Experience experience = instigator.GetComponent<Experience>();
            if (experience == null) return;

            experience.GainExperience(GetComponent<BaseStats>().GetStat(Stat.ExperienceReward));
        }

        private void RegenerateHealth()
        {
            float regenHealthPoints = GetComponent<BaseStats>().GetStat(Stat.Health) * (regenerationPercentage / 100);
            healthPoints.value = Mathf.Max(healthPoints.value, regenHealthPoints);
        }

        public object CaptureState()
        {
            return healthPoints.value;
        }

        public void RestoreState(object state)
        {
            healthPoints.value = (float) state;
            
            if (healthPoints.value <= 0)
            {
                Die();
            }
        }

		[Tooltip("Damage multiplier for part of entity: head - 2.0, hand - 0.5, body - 1.0, etc..")]
		public float entityPartMultiplier = 1f;
		//armor reduces various types of damage
		public Resist armor = new Resist();

		[Space]
		public Shield shield = new Shield();
		public Resist shieldProtection = new Resist();
		public Transform shieldRoot;

		private float totalDamage = 0f;
		private float time = 0f;

		public void Damage(Damage dmg)
		{
			dmg = armor.Reduce(dmg * entityPartMultiplier);
			Debug.Log("hit by special weapon");
			if (shieldRoot != null)
			{
				if (shield.IsBlocked(shieldRoot, dmg.source.weaponTransform.position, transform.position))
				{
					if (dmg.source is IAttackSource && (dmg.source as IAttackSource).attackerTransform != null)
					{
						if (shield.IsBlocked(shieldRoot, (dmg.source as IAttackSource).attackerTransform.position, transform.position))
							dmg = shieldProtection.Reduce(dmg);
					}
					else dmg = shieldProtection.Reduce(dmg);
				}
			}
			TakeDamage(null, dmg.GetSum());
			//counter for convenient display of damage
			totalDamage += dmg.GetSum();
		}
	}
}