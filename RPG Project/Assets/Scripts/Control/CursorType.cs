﻿namespace RPG.Control
{
	public enum CursorType
	{
		None,
		Movement,
		Combat,
		Dialog,
		UI,
		Pickup
	}
}