using RPG.Combat;
using RPG.Movement;
using UnityEngine;
using RPG.Resources;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System;

namespace RPG.Control
{
    public class PlayerController : MonoBehaviour
    {
        Health health;

		[System.Serializable]
		struct CursorMapping
		{
			public CursorType type;

			public Texture2D texture;

			public Vector2 hotspot;
		}

		[SerializeField] CursorMapping[] cursorMappings = null;

		bool doRotation = false;
        private void Awake() {
            health = GetComponent<Health>();
        }

        private void Update()
        {
			if (InteractWithUI()) return;
			
			if (health.IsDead())
			{
				SetCursor(CursorType.None);
				return;
			}
			if (!EventSystem.current.currentSelectedGameObject)
			{
				if (InteractWithNPC()) return;
				//if (InteractWithCombat()) return;
				if (InteractWithComponent()) return;
				if (InteractWithMovement()) return;
				SetCursor(CursorType.None);
			}
        }

		private bool InteractWithUI()
		{
			if (EventSystem.current.IsPointerOverGameObject())
			{
				SetCursor(CursorType.UI);
				return true;
			}
			return false;
		}

		//private bool InteractWithCombat()
  //      {
  //          RaycastHit[] hits = Physics.RaycastAll(GetMouseRay());
  //          foreach (RaycastHit hit in hits)
  //          {
  //              CombatTarget target = hit.transform.GetComponent<CombatTarget>();
		//		//Debug.Log("player found combattarget =" + target);
		//		if (target == null) continue;

  //              if (!GetComponent<Fighter>().CanAttack(target.gameObject))
  //              {
  //                  continue;
  //              }

  //              if (Input.GetMouseButton(0))
  //              {
		//			//if (!IsPointerOverUIObject())
		//			GetComponent<Mover>().SetStoppingDistance(0);
		//			GetComponent<Fighter>().Attack(target.gameObject);
  //              }
		//		SetCursor(CursorType.Combat);
  //              return true;
  //          }
  //          return false;
  //      }

		private bool InteractWithNPC()
		{
			RaycastHit[] hits = Physics.RaycastAll(GetMouseRay());
			foreach (RaycastHit hit in hits)
			{
				NPCTarget target = hit.transform.GetComponent<NPCTarget>();
				//Debug.Log("player found npctarget ="+target);
				if (target == null || !target.isActive) continue;

				if (Input.GetMouseButton(0))
				{
					if (!IsPointerOverUIObject())
					{
						GetComponent<Mover>().SetStoppingDistance(1.5f);
						GetComponent<Mover>().StartMoveAction(target.transform.position, 1f);
						//GetComponent<Mover>().DoRotation(target.transform);
					}
				}
				SetCursor(CursorType.Dialog);
				return true;
			}
			return false;
		}

		private bool InteractWithComponent()
		{
			RaycastHit[] hits = RaycastAllSorted();
			foreach (RaycastHit hit in hits)
			{
				IRaycastable[] raycastables = hit.transform.GetComponents<IRaycastable>();
				foreach (IRaycastable raycastable in raycastables)
				{
					if (raycastable.HandleRaycast(this))
					{
						SetCursor(raycastable.GetCursorType());
						return true;
					}
				}
			}
			return false;
		}

		RaycastHit[] RaycastAllSorted()
		{
			RaycastHit[] hits = Physics.RaycastAll(GetMouseRay());
			float[] distances = new float[hits.Length];
			for (int i = 0; i < hits.Length; i++)
			{
				distances[i] = hits[i].distance;
			}
			Array.Sort(distances, hits);
			return hits;
		}


		private bool InteractWithMovement()
        {
            RaycastHit hit;
            bool hasHit = Physics.Raycast(GetMouseRay(), out hit);
            if (hasHit)
            {
                if (Input.GetMouseButton(0))
                {
					if (!IsPointerOverUIObject())
					{
						GetComponent<Mover>().SetStoppingDistance(0);
						GetComponent<Mover>().StartMoveAction(hit.point, 1f);
					}
                }
				SetCursor(CursorType.Movement);
				return true;
            }
            return false;
        }

		private void SetCursor(CursorType type)
		{
			CursorMapping mapping = GetCursorMapping(type);

			Cursor.SetCursor(mapping.texture, mapping.hotspot, CursorMode.Auto);
		}

		private CursorMapping GetCursorMapping(CursorType type)
		{
			foreach (CursorMapping mapping in cursorMappings)

			{

				if (mapping.type == type)

				{

					return mapping;

				}

			}

			return cursorMappings[0];
		}

		private static Ray GetMouseRay()
        {
            return Camera.main.ScreenPointToRay(Input.mousePosition);
        }

		//When Touching UI
		private bool IsPointerOverUIObject()
		{
			PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
			eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
			List<RaycastResult> results = new List<RaycastResult>();
			EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
			return results.Count > 0;
		}
	}
}