﻿using RPG.Core;
using RPG.Saving;
using UnityEngine;
using UnityEngine.AI;
using RPG.Resources;

namespace RPG.Movement
{
    public class Mover : MonoBehaviour, IAction, ISaveable
    {
        [SerializeField] Transform target;
        [SerializeField] float maxSpeed = 6f;
		[SerializeField] float rotationSpeed = 10f;
		bool _doRotation = false;
		NavMeshAgent navMeshAgent;
        Health health;
		Transform tempTarget;

		private void Awake()
		{
			navMeshAgent = GetComponent<NavMeshAgent>();
			health = GetComponent<Health>();
		}

		void Update()
        {
            navMeshAgent.enabled = !health.IsDead();

            UpdateAnimator();

			//if (_doRotation)
			//{
			//	RotateTo(tempTarget, rotationSpeed);
			//}
        }

        public void StartMoveAction(Vector3 destination, float speedFraction)
        {
            GetComponent<ActionScheduler>().StartAction(this);
            MoveTo(destination, speedFraction);
        }

        public void MoveTo(Vector3 destination, float speedFraction)
        {

			navMeshAgent.destination = destination;
            navMeshAgent.speed = maxSpeed * Mathf.Clamp01(speedFraction);
            navMeshAgent.isStopped = false;
        }

		public void SetStoppingDistance(float stoppingDistance)
		{
			navMeshAgent.stoppingDistance = stoppingDistance;
		}

		//public void DoRotation(Transform target, float speed = 0)
		//{
		//	tempTarget = target;
		//	if (speed > 0)
		//	{
		//		rotationSpeed = speed;
		//	}
		//	navMeshAgent.updateRotation = false;
		//	_doRotation = true;
		//}

		//private void RotateTo(Transform target, float speed)
		//{

		//	Vector3 direction = (target.position - transform.position).normalized;
		//	Quaternion lookRotation = Quaternion.LookRotation(direction);
		//	transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * speed);
		//	print("angle "+ Quaternion.Angle(transform.rotation, lookRotation));
		//	if (Quaternion.Angle(transform.rotation, lookRotation) < 0.5f && Vector3.Distance(target.position, transform.position) <= navMeshAgent.stoppingDistance)
		//	{
		//		_doRotation = false;
		//		navMeshAgent.updateRotation = true;
		//	}

		//}

		public void Cancel()
        {
			navMeshAgent.isStopped = true;
        }

        private void UpdateAnimator()
        {
            Vector3 velocity = navMeshAgent.velocity;
            Vector3 localVelocity = transform.InverseTransformDirection(velocity);
            float speed = localVelocity.z;
            GetComponent<Animator>().SetFloat("forwardSpeed", speed);
        }

        public object CaptureState()
        {
            return new SerializableVector3(transform.position);
        }

		public void RestoreState(object state)
		{
			SerializableVector3 position = (SerializableVector3)state;
			navMeshAgent.enabled = false;
			transform.position = position.ToVector();
			navMeshAgent.enabled = true;
			GetComponent<ActionScheduler>().CancelCurrentAction();
		}
	}
}