﻿using UnityEngine;
using RAD.MeleeSystem;

public class DemoWeapon : MonoBehaviour, IWeaponSource, IAttackSource
{
	public Damage damage = new Damage(null);
	public WeaponMultihitArea area;
	public Transform attackerSource;

	void Start()
	{
		area.onHit += Hit;
		damage.source = this;
	}

	private void Hit(WeaponMultihitArea.HitData[] data)
	{
		if(data.Length < 1) return;
		float damageMultiplier = 0f;
		float distance = data[0].data.distance;
		IDamageReceiver target = null;
		for(int i = 0; i < data.Length; i++)
		{
			damageMultiplier += data[i].data.directionMultiplier;//get summary damage multiplier of all given areas
			if(target == null) target = data[i].hit.collider.GetComponent<IDamageReceiver>();//and get damage receiver
		}
		damageMultiplier /= area.areas.Length;//reduce the total factor to 1.0, dividing by the total number of areas
		if(target != null && damageMultiplier > 0f) target.Damage(damage * (damageMultiplier * distance));//damage is taken from base value, cut distance and the direction multiplier
	}

	public Transform weaponTransform
	{
		get { return transform; }
	}

	public string weaponName
	{
		get { return "Test"; }
	}

	public Transform attackerTransform
	{
		get { return attackerSource; }
	}

	public string attackerName
	{
		get { return "Player"; }
	}
}