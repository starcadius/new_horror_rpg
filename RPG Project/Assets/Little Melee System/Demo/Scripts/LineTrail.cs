﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class LineTrail : MonoBehaviour
{
	private Vertice[] thisVertices = null;
	[SerializeField]
	private Vector3[] _vertices = new Vector3[0];
	public Vector3[] vertices
	{
		get { return _vertices; }
		set
		{
			_vertices = value;
			UpdateVertices();
		}
	}

	public float precision = 0.25f;
	public float smooth = 1f;
	public float delay = 1f;

	[SerializeField]
	private int _maxSections = 10;
	public int maxSections
	{
		get { return _maxSections; }
		set
		{
			if(_maxSections != value)
			{
				_maxSections = value;
				UpdateVertices();
			}
		}
	}

	private MeshFilter _filter;
	private MeshFilter filter
	{
		get
		{
			if(_filter == null)
				_filter = GetComponent<MeshFilter>();
			return _filter;
		}
	}

	private Mesh mesh;
	private Vector3 tempPosition;
	private Vertice tempVertice;
	private List<Vector3> worldVertices = new List<Vector3>();
	private List<Vector3> meshVertices = new List<Vector3>();
	private List<Vector2> meshUV = new List<Vector2>();
	private List<int> meshTriangles = new List<int>();
	private float targetPrecision = float.Epsilon;
	private float currentPrecision = float.Epsilon;

	void Awake()
	{
		mesh = new Mesh();
		mesh.MarkDynamic();
		filter.sharedMesh = mesh;
		UpdateVertices();
	}

	internal void UpdateVertices()
	{
		worldVertices.Clear();
		meshVertices.Clear();
		meshUV.Clear();
		meshTriangles.Clear();
		thisVertices = new Vertice[_vertices.Length];
		for(int i = 0; i < _vertices.Length; i++)
		{
			thisVertices[i] = new Vertice(_vertices[i]);
			AddVertice(thisVertices[i]);
		}
		for(int i = 0; i < thisVertices.Length; i++)
		{
			tempVertice = thisVertices[i];
			meshVertices[tempVertice.index] = tempVertice.localPosition;

			int lastIndex = tempVertice.index;
			int newIndex = AddVertice(tempVertice);
			int leftIndex = -1, rightIndex = -1;

			if(i > 0)
			{
				leftIndex = thisVertices[i - 1].index;
				if(leftIndex == tempVertice.leftIndex)
					leftIndex = AddVertice(thisVertices[i - 1]);
			}
			if(i + 1 < thisVertices.Length)
			{
				rightIndex = thisVertices[i + 1].index;
				if(rightIndex == tempVertice.rightIndex)
					rightIndex = AddVertice(thisVertices[i + 1]);
			}

			tempVertice.leftIndex = leftIndex;
			tempVertice.rightIndex = rightIndex;

			if(leftIndex > -1)
			{
				meshTriangles.Add(leftIndex);
				meshTriangles.Add(lastIndex);
				meshTriangles.Add(newIndex);
			}
			if(rightIndex > -1)
			{
				meshTriangles.Add(lastIndex);
				meshTriangles.Add(rightIndex);
				meshTriangles.Add(newIndex);
			}
		}
	}

	void LateUpdate()
	{
		if(thisVertices == null || thisVertices.Length < 2)
			return;
		for(int i = 0; i < meshVertices.Count; i++)
			meshVertices[i] = transform.InverseTransformPoint(worldVertices[i]);
		currentPrecision = Mathf.Lerp(currentPrecision, targetPrecision, Time.deltaTime / smooth);
		bool fullPrecision = false;
		for(int i = 0; i < thisVertices.Length; i++)
		{
			tempVertice = thisVertices[i];
			tempPosition = transform.TransformPoint(tempVertice.localPosition);
			float dist = Vector3.Distance(tempVertice.lastPosition, tempPosition);
			if(dist / Time.deltaTime >= currentPrecision / 2f)
				fullPrecision = true;
			tempVertice.distance += dist;
			tempVertice.lastPosition = tempPosition;

			if(tempVertice.distance >= currentPrecision || tempVertice.added > -1)
			{
				int lastIndex = tempVertice.added < 0 ? tempVertice.index : tempVertice.indicies[tempVertice.indicies.Count - 1];
				int newIndex = tempVertice.added < 0 ? AddVertice(tempVertice) : tempVertice.index;
				int leftIndex = -1, rightIndex = -1;

				if(i > 0)
				{
					leftIndex = thisVertices[i - 1].index;
					if(thisVertices[i - 1].added != leftIndex && leftIndex == tempVertice.leftIndex)
					{
						leftIndex = AddVertice(thisVertices[i - 1]);
						thisVertices[i - 1].added = newIndex;
					}
				}
				if(i + 1 < thisVertices.Length)
				{
					rightIndex = thisVertices[i + 1].index;
					if(thisVertices[i + 1].added != rightIndex && rightIndex == tempVertice.rightIndex)
					{
						rightIndex = AddVertice(thisVertices[i + 1]);
						thisVertices[i + 1].added = newIndex;
					}
				}

				tempVertice.added = -1;

				tempVertice.leftIndex = leftIndex;
				tempVertice.rightIndex = rightIndex;

				if(leftIndex > -1)
				{
					meshTriangles.Add(leftIndex);
					meshTriangles.Add(lastIndex);
					meshTriangles.Add(newIndex);
				}
				if(rightIndex > -1)
				{
					meshTriangles.Add(lastIndex);
					meshTriangles.Add(rightIndex);
					meshTriangles.Add(newIndex);
				}

				if((rightIndex > -1 || leftIndex > -1) && meshVertices.Count > maxSections * thisVertices.Length)
					RemoveLast(tempVertice);
			}
			else
				meshVertices[tempVertice.index] = tempVertice.localPosition;
		}
		targetPrecision = fullPrecision ? Mathf.Lerp(currentPrecision, precision, Time.deltaTime / smooth) : -1f / delay;
		UpdateUV();
		mesh.SetVertices(meshVertices);
		mesh.SetTriangles(meshTriangles, 0);
		mesh.SetUVs(0, meshUV);
	}

	int AddVertice(Vertice vert)
	{
		vert.distance = 0f;
		vert.lastPosition = transform.TransformPoint(vert.localPosition);

		int index = meshVertices.Count;
		worldVertices.Add(vert.lastPosition);
		meshVertices.Add(vert.localPosition);
		meshUV.Add(Vector2.zero);
		vert.index = index;
		vert.indicies.Add(index);
		return index;
	}

	void RemoveLast(Vertice vert)
	{
		vert.indicies.RemoveAt(0);
		meshVertices.RemoveAt(0);
		worldVertices.RemoveAt(0);
		meshUV.RemoveAt(0);
		if(vert.leftIndex > -1)
			meshTriangles.RemoveRange(0, 3);
		if(vert.rightIndex > -1)
			meshTriangles.RemoveRange(0, 3);

		UpdateIndicies();
	}

	void UpdateIndicies()
	{
		for(int i = 0; i < meshTriangles.Count; i++)
			Mathf.Max(meshTriangles[i]--, 0);
		Vertice vert;
		for(int i = 0; i < thisVertices.Length; i++)
		{
			vert = thisVertices[i];
			vert.index--;
			vert.leftIndex--;
			vert.rightIndex--;
			for(int j = 0; j < vert.indicies.Count; j++)
				Mathf.Max(vert.indicies[j]--, 0);
		}
	}

	void UpdateUV()
	{
		float hstep = 1f / (thisVertices.Length - 1);
		Vertice vert;
		float vstep;
		int count;
		for(int i = 0; i < thisVertices.Length; i++)
		{
			vert = thisVertices[i];
			count = vert.indicies.Count - 1;
			vstep = 1f / (count - 1);
			for(int j = 0; j <= count; j++)
			{
				if(j == 0)
					meshUV[vert.indicies[j]] = new Vector2(i * hstep, 1f);
				else
					meshUV[vert.indicies[j]] = new Vector2(i * hstep, 1f - Mathf.Clamp01((j - 1) * vstep + (1f - vert.distance / currentPrecision) * vstep));
			}
		}
	}

#if UNITY_EDITOR
	void OnDrawGizmos()
	{
		Gizmos.color = Color.green;
		for(int i = 0; i < _vertices.Length; i++)
		{
			Gizmos.DrawSphere(transform.TransformPoint(_vertices[i]), 0.01f);
		}
	}
#endif

	private class Vertice
	{
		public float distance;
		public Vector3 lastPosition;
		public Vector3 localPosition;
		public int index;
		public int leftIndex;
		public int rightIndex;
		public List<int> indicies;
		public int added = -1;

		public Vertice(Vector3 localPosition)
		{
			this.localPosition = localPosition;
			lastPosition = localPosition;
			distance = 0f;
			leftIndex = -1;
			rightIndex = -1;
			index = -1;
			indicies = new List<int>();
		}
	}
}
