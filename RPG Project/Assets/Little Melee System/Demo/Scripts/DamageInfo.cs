﻿using RAD.MeleeSystem;
using UnityEngine;

public class DamageInfo : MonoBehaviour, IDamageReceiver
{
	[Tooltip("Damage multiplier for part of entity: head - 2.0, hand - 0.5, body - 1.0, etc..")]
	public float entityPartMultiplier = 1f;
	//armor reduces various types of damage
	public Resist armor = new Resist();

	[Space]
	public Shield shield = new Shield();
	public Resist shieldProtection = new Resist();
	public Transform shieldRoot;

	private float totalDamage = 0f;
	private float time = 0f;

	public void Damage(Damage dmg)
	{
		dmg = armor.Reduce(dmg * entityPartMultiplier);

		if(shieldRoot != null)
		{
			if(shield.IsBlocked(shieldRoot, dmg.source.weaponTransform.position, transform.position))
			{
				if(dmg.source is IAttackSource && (dmg.source as IAttackSource).attackerTransform != null)
				{
					if(shield.IsBlocked(shieldRoot, (dmg.source as IAttackSource).attackerTransform.position, transform.position))
						dmg = shieldProtection.Reduce(dmg);
				}
				else dmg = shieldProtection.Reduce(dmg);
			}
		}

		//counter for convenient display of damage
		totalDamage += dmg.GetSum();
	}

	void OnDrawGizmos()
	{
		if(shieldRoot != null)
		{
			shield.GizmosDraw(shieldRoot);
		}
	}

	void Update()
	{
		time += Time.deltaTime;
		if(time >= 0.25f)
		{
			time = 0f;
			if(totalDamage > 0f)
			{
				DamageInfoDrawer.AddInfo(totalDamage.ToString("F1"), Color.red, transform.position, Camera.main.transform.position);
				totalDamage = 0f;
			}
		}
	}
}