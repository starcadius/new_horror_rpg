﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DamageInfoDrawer : MonoBehaviour
{
	public Text prefab;
	public Vector3 moveVector = Vector3.up;
	public float hideTime = 1f;

	private List<Text> activeList = new List<Text>();
	private List<Text> inactiveList = new List<Text>();

	private Color tempColor;
	private Text tempText;

	private static DamageInfoDrawer instance;

	public static void AddInfo(string text, Color color, Vector3 pos, Vector3 look)
	{
		if(instance == null)
			return;
		instance.SpawnInfo(text, color, pos, look);
	}

	void Awake()
	{
		instance = this;
	}

	void OnDestroy()
	{
		instance = null;
	}

	void SpawnInfo(string text, Color color, Vector3 pos, Vector3 look)
	{
		Text tempText;
		if(inactiveList.Count > 0)
		{
			tempText = inactiveList[0];
			tempText.gameObject.SetActive(true);
			inactiveList.RemoveAt(0);
		}
		else
		{
			tempText = Instantiate(prefab);
			tempText.transform.SetParent(transform);
			tempText.transform.localScale = prefab.transform.localScale;
		}

		tempText.transform.position = pos;
		tempText.transform.LookAt(pos - (look - pos));
		tempText.color = color;
		tempText.text = text;

		activeList.Add(tempText);
	}

	void Update()
	{
		for(int i = activeList.Count - 1; i > -1; i--)
		{
			tempText = activeList[i];
			tempColor = tempText.color;
			if(tempColor.a <= 0f)
			{
				activeList.RemoveAt(i);
				tempText.gameObject.SetActive(false);
				inactiveList.Add(tempText);
				tempText = null;
				continue;
			}
			tempText.transform.position += moveVector * Time.deltaTime;
			tempColor.a -= Time.deltaTime / hideTime;
			if(tempColor.a < 0f) tempColor.a = 0f;
			tempText.color = tempColor;
		}
	}
}