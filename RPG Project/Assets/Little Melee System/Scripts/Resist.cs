﻿using UnityEngine;

namespace RAD.MeleeSystem
{
	[System.Serializable]
	public class Resist
	{
		public Pair[] resists;

		public Resist(params Pair[] resists)
		{
			this.resists = resists;
		}

		/// <summary>
		/// Calculates reduced damage from own resists.
		/// Returns a new `Damage` instance with recalculated values.
		/// </summary>
		public Damage Reduce(Damage damage)
		{
			Damage.Pair[] reduced = new Damage.Pair[damage.values.Length];

			for(int i = 0; i < damage.values.Length; i++)
			{
				reduced[i] = damage.values[i];
				for(int j = 0; j < resists.Length; j++)
				{
					if(resists[j].type == damage.values[i].type)
					{
						reduced[i].value *= (1f - resists[j].value);
						break;
					}
				}
			}

			return new Damage(damage.source, reduced);
		}

		/// <summary>
		/// Container for typed resist
		/// Max resist value - 1.0 (100% resist)
		/// </summary>
		[System.Serializable]
		public struct Pair
		{
			public DamageType type;

			[SerializeField]
			private float _value;
			public float value
			{
				get { return _value; }
				set { _value = Mathf.Clamp01(value); }
			}

			public Pair(float value, DamageType type)
			{
				_value = Mathf.Clamp01(value);
				this.type = type;
			}
		}
	}
}