﻿namespace RAD.MeleeSystem
{
	[System.Obsolete("Use IWeaponSource (and IAttackSource) to get the `damage source`.", true)]
	public interface IDamageSource
	{
		[System.Obsolete("Use IWeaponSource.weaponTransform to get weapon transform.")]
		UnityEngine.Transform transform { get; }
		[System.Obsolete("Use IAttackSource.attackerName to get attacker name.")]
		string sourceName { get; }
		string weaponName { get; }
	}

	public interface IWeaponSource
	{
		UnityEngine.Transform weaponTransform { get; }
		string weaponName { get; }
	}

	public interface IAttackSource
	{
		UnityEngine.Transform attackerTransform { get; }
		string attackerName { get; }
	}


	public interface IDamageReceiver
	{
		void Damage(Damage dmg);
	}
}