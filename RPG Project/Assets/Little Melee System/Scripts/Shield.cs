﻿using UnityEngine;

namespace RAD.MeleeSystem
{
	/// <summary>
	/// Simple shield - is a plane with methods that determine whether a point on the plane belongs to some geometric figure.
	/// </summary>
	[System.Serializable]
	public class Shield
	{
		public ShieldType shieldType = ShieldType.Rectangle;
		public Vector2 rectangleHalfSize = Vector2.one;
		public float radius = 1f;

#if UNITY_EDITOR
		public void GizmosDraw(Transform shieldRoot)
		{
			UnityEditor.Handles.color = Color.yellow;
			switch(shieldType)
			{
				case ShieldType.Rectangle:
					UnityEditor.Handles.DrawLines(new Vector3[] {
					shieldRoot.TransformPoint(new Vector3(-rectangleHalfSize.x, -rectangleHalfSize.y, 0f)),
					shieldRoot.TransformPoint(new Vector3(rectangleHalfSize.x, -rectangleHalfSize.y, 0f)),
					shieldRoot.TransformPoint(new Vector3(rectangleHalfSize.x, rectangleHalfSize.y, 0f)),
					shieldRoot.TransformPoint(new Vector3(-rectangleHalfSize.x, rectangleHalfSize.y, 0f))
				}, new int[] { 0, 1, 1, 2, 2, 3, 3, 0 });
					break;
				case ShieldType.Circle:
					UnityEditor.Handles.DrawWireDisc(shieldRoot.position, shieldRoot.forward, radius * Vector3.Dot(shieldRoot.lossyScale, Vector3.one / 3f));
					break;
			}
		}
#endif

		public bool IsBlocked(Transform shieldRoot, Vector3 source, Vector3 target)
		{
			Vector3 direction = shieldRoot.forward;
			Vector3 crossPoint;
			if(CrossPlane(shieldRoot.position, direction, source, target, out crossPoint))
			{
				switch(shieldType)
				{
					case ShieldType.Rectangle:
						return InRectangle(crossPoint, new Vector3[] {
						shieldRoot.TransformPoint(new Vector3(-rectangleHalfSize.x, -rectangleHalfSize.y, 0f)),
						shieldRoot.TransformPoint(new Vector3(rectangleHalfSize.x, -rectangleHalfSize.y, 0f)),
						shieldRoot.TransformPoint(new Vector3(rectangleHalfSize.x, rectangleHalfSize.y, 0f)),
						shieldRoot.TransformPoint(new Vector3(-rectangleHalfSize.x, rectangleHalfSize.y, 0f))
					});
					case ShieldType.Circle:
						return Vector3.Distance(shieldRoot.position, crossPoint) <= radius * Vector3.Dot(shieldRoot.lossyScale, Vector3.one / 3f);
				}
			}
			return false;
		}

		public bool IsBlocked(Transform shieldRoot, Vector3 source, Vector3 target, out Vector3 blockPoint)
		{
			Vector3 direction = shieldRoot.forward;
			Vector3 crossPoint;
			if(CrossPlane(shieldRoot.position, direction, source, target, out crossPoint))
			{
				blockPoint = crossPoint;
				switch(shieldType)
				{
					case ShieldType.Rectangle:
						return InRectangle(crossPoint, new Vector3[] {
						shieldRoot.TransformPoint(new Vector3(-rectangleHalfSize.x, -rectangleHalfSize.y, 0f)),
						shieldRoot.TransformPoint(new Vector3(rectangleHalfSize.x, -rectangleHalfSize.y, 0f)),
						shieldRoot.TransformPoint(new Vector3(rectangleHalfSize.x, rectangleHalfSize.y, 0f)),
						shieldRoot.TransformPoint(new Vector3(-rectangleHalfSize.x, rectangleHalfSize.y, 0f))
					});
					case ShieldType.Circle:
						return Vector3.Distance(shieldRoot.position, crossPoint) <= radius * Vector3.Dot(shieldRoot.lossyScale, Vector3.one / 3f);
				}
			}
			blockPoint = Vector3.zero;
			return false;
		}

		/// <summary>
		/// Returns true if the point belongs to a rectangle.
		/// </summary>
		private bool InRectangle(Vector3 point, Vector3[] vertices)
		{
			float rwidth = Vector3.Distance(vertices[0], vertices[1]);
			float rheight = Vector3.Distance(vertices[1], vertices[2]);

			float a = Vector3.Distance(vertices[0], point);
			float b = Vector3.Distance(vertices[1], point);
			float c = Vector3.Distance(vertices[2], point);
			float d = Vector3.Distance(vertices[3], point);
			// To verify the belonging, it is necessary to calculate the area of the rectangle and 4 triangles,
			// formed when the lines from corners are drawn to the point.
			//
			// width h
			// *---* e
			// |\a/| i
			// |d*b| g
			// |/c\| h
			// *---* t
			return rwidth * rheight >= (TriangleArea(a, b, rwidth) + TriangleArea(b, c, rheight) +
				TriangleArea(c, d, rwidth) + TriangleArea(d, a, rheight)) * 0.99f;//0.99 - float error
		}

		/// <summary>
		/// Heron's Formula
		/// </summary>
		private float TriangleArea(float a, float b, float c)
		{
			float p = (a + b + c) / 2f;
			return Mathf.Sqrt(p * (p - a) * (p - b) * (p - c));
		}

		public static bool CrossPlane(Vector3 planePoint, Vector3 planeNormal, Vector3 pointA, Vector3 pointB, out Vector3 pos)
		{
			Vector3 ABVector = pointB - pointA;
			Vector3 APVector = planePoint - pointA;
			float l = Vector3.Dot(APVector, planeNormal) / Vector3.Dot(ABVector, planeNormal);

			pos = Vector3.zero;
			if(l < 0f || l > 1f) return false;

			pos = Vector3.Lerp(pointA, pointB, l);
			return true;
		}

		public enum ShieldType
		{
			Rectangle = 0,
			Circle = 1
		}
	}
}