﻿using System.Collections.Generic;
using UnityEngine;

namespace RAD.MeleeSystem
{
	public class WeaponMultihitArea : MonoBehaviour
	{
		/// <summary>
		/// Layer mask of casts
		/// </summary>
		public LayerMask layer;

		[SerializeField]
		private HitArea[] _areas = new HitArea[0];
		public HitArea[] areas
		{
			get { return _areas; }
			set { _areas = value; areasData = new AreaData[areas.Length]; }
		}

		/// <param name="data">Hits on collider</param>
		public delegate void OnHit(HitData[] data);
		/// <summary>
		/// Hit separately for each collider
		/// </summary>
		public event OnHit onHit;

		public delegate void OnAreasFrame(AreaData[] data);
		/// <summary>
		/// Get basic areas data (position, direction, etc...) per frame
		/// </summary>
		public event OnAreasFrame onAreasFrame;

		/// <summary>
		/// Take hits toggle
		/// </summary>
		public bool active = false;

		/// <summary>
		/// Calculates a certain coefficient based on the motion vector and the cast direction.
		/// </summary>
		public bool calcDirection = false;
		/// <summary>
		/// Max deviation of motion vector. 1 is 180 degrees.
		/// </summary>
		public float maxDeviation = 1f;

		private RaycastHit[] hits;
		private AreaData data;
		private List<HitData> dataList;
		private Dictionary<Collider, List<HitData>> hitsData;
		private AreaData[] areasData;

		void OnEnable()
		{
			for(int i = 0; i < areas.Length; i++)
				areas[i].UpdatePosition(transform);
			hitsData = new Dictionary<Collider, List<HitData>>(areas.Length);
			areasData = new AreaData[areas.Length];
		}

		void OnDisable()
		{
			hits = null;
			dataList = null;
			hitsData = null;
			onHit = null;
		}

		void OnDestroy()
		{
			hits = null;
			dataList = null;
			hitsData = null;
			onHit = null;
		}

		void FixedUpdate()
		{
			if(active)
			{
				if(onAreasFrame != null)
				{
					for(int i = 0; i < areas.Length; i++)
						areasData[i] = areas[i].GetAreaData(transform);
					onAreasFrame.Invoke(areasData);
				}
				if(onHit != null)
				{
					hitsData.Clear();
					for(int i = 0; i < areas.Length; i++)
					{
						if(areas[i].GetHits(transform, calcDirection, maxDeviation, layer, out data, out hits))
						{
							for(int j = 0; j < hits.Length; j++)
							{
								if(hitsData.TryGetValue(hits[j].collider, out dataList))
									dataList.Add(new HitData(hits[j], data));
								else
									hitsData.Add(hits[j].collider, new List<HitData>(areas.Length) { new HitData(hits[j], data) });
							}
						}
					}
					foreach(var hd in hitsData)
					{
						onHit.Invoke(hd.Value.ToArray());
					}
				}
			}
			for(int i = 0; i < areas.Length; i++)
				areas[i].UpdatePosition(transform);
		}

#if UNITY_EDITOR
		void OnDrawGizmos()
		{
			bool selected = UnityEditor.Selection.activeObject == gameObject;
			for(int i = 0; i < areas.Length; i++)
			{
				areas[i].transform = transform;
				areas[i].drawDirection = selected && calcDirection;
				areas[i].DrawGizmos();
			}
		}
#endif

		public class HitData
		{
			public readonly RaycastHit hit;
			public readonly AreaData data;

			public HitData(RaycastHit hit, AreaData data)
			{
				this.hit = hit;
				this.data = data;
			}
		}
	}
}