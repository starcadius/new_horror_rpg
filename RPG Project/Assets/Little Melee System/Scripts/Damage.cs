﻿namespace RAD.MeleeSystem
{
	[System.Serializable]
	public class Damage
	{
		public Pair[] values;
		public IWeaponSource source;

		public Damage(IWeaponSource source, params Pair[] values)
		{
			this.values = values;
			this.source = source;
		}

		public void Multiply(float mult)
		{
			for(int i = 0; i < values.Length; i++)
			{
				values[i].value *= mult;
			}
		}

		public float GetSum()
		{
			float sum = 0f;
			for(int i = 0; i < values.Length; i++)
			{
				sum += values[i].value;
			}
			return sum;
		}

		public static Damage operator *(Damage damage, float mult)
		{
			Pair[] values = new Pair[damage.values.Length];
			for(int i = 0; i < values.Length; i++)
			{
				values[i] = new Pair(damage.values[i].value * mult, damage.values[i].type);
			}
			return new Damage(damage.source, values);
		}

		/// <summary>
		/// Container for typed damage
		/// </summary>
		[System.Serializable]
		public struct Pair
		{
			public DamageType type;
			public float value;

			public Pair(float value, DamageType type)
			{
				this.value = value;
				this.type = type;
			}
		}
	}
}