﻿using UnityEngine;

namespace RAD.MeleeSystem
{
	public class WeaponHitArea : MonoBehaviour
	{
		/// <summary>
		/// Layer mask of casts
		/// </summary>
		public LayerMask layer;

		public HitArea area = new HitArea();

		public delegate void OnHit(RaycastHit hit, AreaData data);
		/// <summary>
		/// Hit separately for each collider
		/// </summary>
		public event OnHit onHit;

		public delegate void OnAreaFrame(AreaData data);
		/// <summary>
		/// Get basic area data (position, direction, etc...) per frame
		/// </summary>
		public event OnAreaFrame onAreaFrame;

		/// <summary>
		/// Take hits toggle
		/// </summary>
		public bool active = false;

		/// <summary>
		/// Calculates a certain coefficient based on the motion vector and the blade direction.
		/// </summary>
		public bool calcDirection = false;
		/// <summary>
		/// Max deviation of motion vector. 1 is 180 degrees.
		/// </summary>
		public float maxDeviation = 1f;

		void OnEnable()
		{
			area.UpdatePosition(transform);
		}

		void OnDisable()
		{
			onHit = null;
		}

		void OnDestroy()
		{
			onHit = null;
		}

		void FixedUpdate()
		{
			if(active)
			{
				if(onAreaFrame != null)
				{
					onAreaFrame.Invoke(area.GetAreaData(transform));
				}
				if(onHit != null)
				{
					RaycastHit[] hits;
					AreaData data;
					if(area.GetHits(transform, calcDirection, maxDeviation, layer, out data, out hits))
					{
						for(int i = 0; i < hits.Length; i++)
							onHit.Invoke(hits[i], data);
					}
				}
			}
			area.UpdatePosition(transform);
		}

#if UNITY_EDITOR
		void OnDrawGizmos()
		{
			bool selected = UnityEditor.Selection.activeObject == gameObject;
			area.drawDirection = selected && calcDirection;
			area.transform = transform;
			area.DrawGizmos();
		}
#endif
	}
}