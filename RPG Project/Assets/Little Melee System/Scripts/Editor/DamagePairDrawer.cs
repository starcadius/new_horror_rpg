﻿using UnityEditor;
using UnityEngine;

namespace RAD.MeleeSystem
{
	[CustomPropertyDrawer(typeof(Damage.Pair))]
	public class DamagePairDrawer : PropertyDrawer
	{
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			EditorGUI.BeginProperty(position, label, property);

			Rect rect = EditorGUI.PrefixLabel(position, label);

			int level = EditorGUI.indentLevel;//fix auto indent
			EditorGUI.indentLevel = 0;

			rect.width /= 2f;
			EnumProperty(rect, property.FindPropertyRelative("type"));
			rect.x += rect.width;
			FloatProperty(rect, property.FindPropertyRelative("value"));

			EditorGUI.indentLevel = level;
			EditorGUI.EndProperty();
		}

		private static void EnumProperty(Rect position, SerializedProperty property)
		{
			EditorGUI.BeginChangeCheck();
			int enumValueIndex = EditorGUI.Popup(position, property.enumValueIndex, property.enumDisplayNames);
			if(EditorGUI.EndChangeCheck())
				property.enumValueIndex = enumValueIndex;
		}

		private static void FloatProperty(Rect position, SerializedProperty property)
		{
			EditorGUI.BeginChangeCheck();

			float floatValue = EditorGUI.FloatField(position, property.floatValue);

			if(EditorGUI.EndChangeCheck())
				property.floatValue = floatValue;
		}
	}
}