﻿using UnityEditor;
using UnityEngine;

namespace RAD.MeleeSystem
{
	[CustomPropertyDrawer(typeof(Shield))]
	public class ShieldDrawer : PropertyDrawer
	{
		private static GUIContent rectangleSizeLabel = new GUIContent("Half size");

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			float height = 18f;
			if(property.isExpanded)
			{
				var type = property.FindPropertyRelative("shieldType");
				height += EditorGUI.GetPropertyHeight(type);
				switch((Shield.ShieldType)type.enumValueIndex)
				{
					case Shield.ShieldType.Rectangle:
						height += EditorGUI.GetPropertyHeight(property.FindPropertyRelative("rectangleHalfSize"));
						break;
					case Shield.ShieldType.Circle:
						height += EditorGUI.GetPropertyHeight(property.FindPropertyRelative("radius"));
						break;
				}
			}
			return height;
		}

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			property.isExpanded = EditorGUI.Foldout(new Rect(position.x, position.y, position.width, 18f), property.isExpanded, label);
			if(property.isExpanded)
			{
				position.y += 18f;
				position.height -= 18f;
				var type = property.FindPropertyRelative("shieldType");
				float height = EditorGUI.GetPropertyHeight(type);
				EditorGUI.PropertyField(new Rect(position.x, position.y, position.width, height), type);
				position.y += height;
				position.height -= height;

				switch((Shield.ShieldType)type.enumValueIndex)
				{
					case Shield.ShieldType.Rectangle:
						EditorGUI.PropertyField(position, property.FindPropertyRelative("rectangleHalfSize"), rectangleSizeLabel);
						break;
					case Shield.ShieldType.Circle:
						EditorGUI.PropertyField(position, property.FindPropertyRelative("radius"));
						break;
				}
			}
		}
	}
}