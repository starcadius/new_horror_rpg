﻿using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace RAD.MeleeSystem
{
	[CustomEditor(typeof(WeaponHitArea)), CanEditMultipleObjects]
	internal class WeaponHitAreaInspector : Editor
	{
		private static readonly GUIContent activeName = new GUIContent("Active", "Calculation of hits is active.");
		private static readonly GUIContent calcDirectionName = new GUIContent("Calculate direction", "Calculates a certain coefficient based on the motion vector and the blade direction.\n(Toggle to show or hide additional properties)");
		private static readonly GUIContent maxDeviationName = new GUIContent("Deviation", "Max deviation of motion vector. 1 is 180 degrees.");

		#region change check
		private static Vector3 offsetBack;
		private static Vector3 offset2Back;
		private static Vector3 sizeBack;
		private static Vector3 directionBack;
		private static Vector3 rotationBack;
		private static float radiusBack;

		internal static void SetParams(HitArea hitArea)
		{
			offsetBack = hitArea.offset;
			offset2Back = hitArea.offset2;
			sizeBack = hitArea.halfExtents;
			directionBack = hitArea.direction;
			rotationBack = hitArea.orientation;
			radiusBack = hitArea.radius;
		}

		/// <summary>
		/// Checks for changes of HitArea and adds 'undo action' if a change is detected.
		/// </summary>
		internal static void ChangeCheck(HitArea hitArea, Object obj)
		{
			if(offsetBack != hitArea.offset || offset2Back != hitArea.offset2 || sizeBack != hitArea.halfExtents ||
				directionBack != hitArea.direction || rotationBack != hitArea.orientation || radiusBack != hitArea.radius)
			{
				Vector3 offsetChange = hitArea.offset;
				Vector3 offset2Change = hitArea.offset2;
				Vector3 sizeChange = hitArea.halfExtents;
				Vector3 directionChange = hitArea.direction;
				Vector3 rotationChange = hitArea.orientation;
				float radiusChange = hitArea.radius;

				hitArea.offset = offsetBack;
				hitArea.offset2 = offset2Back;
				hitArea.halfExtents = sizeBack;
				hitArea.direction = directionBack;
				hitArea.orientation = rotationBack;
				hitArea.radius = radiusBack;

				Undo.RecordObject(obj, "Modify Hit Area");

				hitArea.offset = offsetChange;
				hitArea.offset2 = offset2Change;
				hitArea.halfExtents = sizeChange;
				hitArea.direction = directionChange;
				hitArea.orientation = rotationChange;
				hitArea.radius = radiusChange;
			}
		}
		#endregion

		private EditProperty editHitArea = EditProperty.None;
		private Quaternion direction;

		public void OnDisable()
		{
			(this.target as WeaponHitArea).area.draw = true;
			if(editHitArea != EditProperty.None) ChangeCheck((this.target as WeaponHitArea).area, target);
		}

		public override void OnInspectorGUI()
		{
			EditProperty prev = editHitArea;
			WeaponHitAreaInspector.DrawInspector(serializedObject, this, ref editHitArea);

			HitArea hitArea = (this.target as WeaponHitArea).area;
			hitArea.draw = hitArea.castType != CastType.Sphere || editHitArea != EditProperty.Radius;

			if(prev != editHitArea)
			{
				if(prev != EditProperty.None) ChangeCheck((this.target as WeaponHitArea).area, target);
				else SetParams(hitArea);
				if(editHitArea == EditProperty.Direction)
				{
					Transform transform = (this.target as WeaponHitArea).transform;
					if(hitArea.direction == Vector3.zero) hitArea.direction = Vector3.forward;
					direction = transform.rotation * Quaternion.LookRotation(hitArea.direction, transform.forward);
				}
			}
		}

		internal static int DrawInspector(SerializedObject serializedObject, Editor editor, ref EditProperty editHitArea, int index = -1)
		{
			serializedObject.Update();
			SerializedProperty iterator = serializedObject.GetIterator();
			bool enterChildren = true;
			while(iterator.NextVisible(enterChildren))
			{
				enterChildren = false;
				switch(iterator.name)
				{
					case "active":
						EditorGUILayout.PropertyField(iterator, activeName, true, new GUILayoutOption[0]);
						break;
					case "calcDirection":
						EditorGUILayout.PropertyField(iterator, calcDirectionName, true, new GUILayoutOption[0]);
						break;
					case "maxDeviation":
						if(serializedObject.FindProperty("calcDirection").boolValue)
							EditorGUILayout.Slider(iterator, float.Epsilon, 1f, maxDeviationName);
						break;
					case "area":
						HitAreaDrawer.DrawEditorProperty(iterator, new GUIContent(iterator.displayName), editor, ref editHitArea);
						break;
					case "_areas":
						Rect rect = EditorGUILayout.GetControlRect(true);
						GUIContent label = EditorGUI.BeginProperty(rect, null, iterator);
						iterator.isExpanded = EditorGUI.Foldout(rect, iterator.isExpanded, label);
						if(iterator.isExpanded)
						{
							EditorGUI.indentLevel++;
							iterator.arraySize = EditorGUILayout.DelayedIntField("Size", iterator.arraySize);
							SerializedProperty prop;
							EditProperty temp, prev;
							for(int i = 0; i < iterator.arraySize; i++)
							{
								prop = iterator.GetArrayElementAtIndex(i);
								temp = editHitArea;
								if(index != i) temp = EditProperty.None;
								prev = temp;
								HitAreaDrawer.DrawEditorProperty(prop, new GUIContent(prop.displayName), editor, ref temp);
								if(prev != temp)
								{
									editHitArea = temp;
									index = i;
								}
							}
							EditorGUI.indentLevel--;
						}
						EditorGUI.EndProperty();
						break;
					default:
						using(new EditorGUI.DisabledScope("m_Script" == iterator.propertyPath))
						{
							EditorGUILayout.PropertyField(iterator, true, new GUILayoutOption[0]);
						}
						break;
				}
			}
			serializedObject.ApplyModifiedProperties();
			return index;
		}

		#region scene editor
		public void OnSceneGUI()
		{
			if(editHitArea != EditProperty.None)
			{
				HitArea hitArea = (this.target as WeaponHitArea).area;
				Transform transform = (this.target as WeaponHitArea).transform;
				DrawEditor(editHitArea, hitArea, transform, ref direction);
			}
		}

		internal static bool DrawEditor(EditProperty editHitArea, HitArea hitArea, Transform transform, ref Quaternion direction)
		{
			Color color = Handles.color;
			Handles.color = Color.green;
			Quaternion rot = Tools.pivotRotation == PivotRotation.Global ? Quaternion.identity : transform.rotation;
			if(hitArea.castType == CastType.Box)
			{
				if(editHitArea == EditProperty.Rotation || editHitArea == EditProperty.Size || Tools.pivotRotation != PivotRotation.Global)
					rot = transform.rotation * Quaternion.Euler(hitArea.orientation);
			}
			switch(editHitArea)
			{
				case EditProperty.Offset:
					hitArea.offset = transform.InverseTransformPoint(Handles.PositionHandle(transform.TransformPoint(hitArea.offset), rot));
					break;
				case EditProperty.Offset2:
					hitArea.offset2 = transform.InverseTransformPoint(Handles.PositionHandle(transform.TransformPoint(hitArea.offset2), rot));
					break;
				case EditProperty.Size:
					hitArea.halfExtents = Handles.ScaleHandle(hitArea.halfExtents, transform.TransformPoint(hitArea.offset),
						rot, HandleUtility.GetHandleSize(transform.TransformPoint(hitArea.offset)));
					break;
				case EditProperty.Rotation:
					hitArea.orientation = (Quaternion.Inverse(transform.rotation) * Handles.RotationHandle(transform.rotation * Quaternion.Euler(hitArea.orientation),
						transform.TransformPoint(hitArea.offset))).eulerAngles;
					break;
				case EditProperty.Radius:
					hitArea.radius = Handles.RadiusHandle(Quaternion.identity, transform.TransformPoint(hitArea.offset), hitArea.radius);
					break;
				case EditProperty.Direction:
					direction = Handles.RotationHandle(direction, transform.TransformPoint(hitArea.offset));
					hitArea.direction = (Quaternion.Inverse(transform.rotation) * direction) * Vector3.forward;
					break;
			}
			Handles.color = color;
			return false;
		}

		internal enum EditProperty
		{
			None = 0,
			Offset = 1,
			Size = 2,
			Radius = 3,
			Rotation = 4,
			Direction = 5,
			Offset2 = 6
		}
		#endregion
	}

	[CustomEditor(typeof(WeaponMultihitArea)), CanEditMultipleObjects]
	internal class WeaponMultihitAreaInspector : Editor
	{
		private WeaponHitAreaInspector.EditProperty editHitArea = WeaponHitAreaInspector.EditProperty.None;
		private int index = -1;
		private Quaternion direction;

		public void OnDisable()
		{
			if(index > -1 && index < (this.target as WeaponMultihitArea).areas.Length)
			{
				HitArea hitArea = (this.target as WeaponMultihitArea).areas[index];
				hitArea.draw = true;
				if(editHitArea != WeaponHitAreaInspector.EditProperty.None) WeaponHitAreaInspector.ChangeCheck(hitArea, target);
			}
		}

		public override void OnInspectorGUI()
		{
			WeaponHitAreaInspector.EditProperty prev = editHitArea;
			index = WeaponHitAreaInspector.DrawInspector(serializedObject, this, ref editHitArea, index);

			if(index > -1 && index < (this.target as WeaponMultihitArea).areas.Length)
			{
				HitArea hitArea = (this.target as WeaponMultihitArea).areas[index];
				hitArea.draw = hitArea.castType != CastType.Sphere || editHitArea != WeaponHitAreaInspector.EditProperty.Radius;

				if(prev != editHitArea)
				{
					if(prev != WeaponHitAreaInspector.EditProperty.None) WeaponHitAreaInspector.ChangeCheck(hitArea, target);
					else WeaponHitAreaInspector.SetParams(hitArea);

					if(editHitArea == WeaponHitAreaInspector.EditProperty.Direction)
					{
						Transform transform = (this.target as WeaponMultihitArea).transform;
						if(hitArea.direction == Vector3.zero) hitArea.direction = Vector3.forward;
						direction = transform.rotation * Quaternion.LookRotation(hitArea.direction, transform.forward);
					}
				}
			}
		}

		public void OnSceneGUI()
		{
			if(editHitArea != WeaponHitAreaInspector.EditProperty.None && index > -1 && index < (this.target as WeaponMultihitArea).areas.Length)
			{
				HitArea hitArea = (this.target as WeaponMultihitArea).areas[index];
				Transform transform = (this.target as WeaponMultihitArea).transform;
				WeaponHitAreaInspector.DrawEditor(editHitArea, hitArea, transform, ref direction);
			}
		}
	}
}