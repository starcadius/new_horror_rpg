﻿using UnityEditor;
using UnityEngine;

namespace RAD.MeleeSystem
{
	[CustomPropertyDrawer(typeof(Resist))]
	public class ResistDrawer : PropertyDrawer
	{
		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			return EditorGUI.GetPropertyHeight(property.FindPropertyRelative("resists"), label);
		}

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			EditorGUI.PropertyField(position, property.FindPropertyRelative("resists"), label, true);
		}
	}
}