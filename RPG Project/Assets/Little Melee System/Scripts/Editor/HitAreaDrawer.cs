﻿using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using EditProperty = RAD.MeleeSystem.WeaponHitAreaInspector.EditProperty;

namespace RAD.MeleeSystem
{
	[CustomPropertyDrawer(typeof(HitArea), true)]
	public class HitAreaDrawer : PropertyDrawer
	{
		private static readonly GUIContent point1Name = new GUIContent("Position 1");
		private static readonly GUIContent point2Name = new GUIContent("Position 2");
		private static readonly GUIContent directionName = new GUIContent("Blade direction");
		private static readonly GUIContent halfSizeName = new GUIContent("Half extents");
		private static readonly GUIContent editToggleContent = new GUIContent("edit", "Edit area position and form");

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			float height = 18f;
			if(property.isExpanded)
			{
				var prop = property.FindPropertyRelative("castType");
				height += EditorGUI.GetPropertyHeight(prop) + 2f;
				height += EditorGUI.GetPropertyHeight(property.FindPropertyRelative("offset")) + 2f;
				switch(prop.enumValueIndex)
				{
					case 1:
						height += EditorGUI.GetPropertyHeight(property.FindPropertyRelative("radius")) + 2f;
						break;
					case 2:
						height += EditorGUI.GetPropertyHeight(property.FindPropertyRelative("radius")) + 2f;
						height += EditorGUI.GetPropertyHeight(property.FindPropertyRelative("size")) + 2f;
						break;
					case 3:
						height += EditorGUI.GetPropertyHeight(property.FindPropertyRelative("size")) + 2f;
						height += EditorGUI.GetPropertyHeight(property.FindPropertyRelative("orientation")) + 2f;
						break;
				}
				if((prop = property.serializedObject.FindProperty("calcDirection")) != null && prop.boolValue)
					height += EditorGUI.GetPropertyHeight(property.FindPropertyRelative("direction")) + 2f;
			}
			return height;
		}

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			position = EditorGUI.IndentedRect(position);

			label = EditorGUI.BeginProperty(position, label, property);

			Rect rect = position;
			rect.height = 16f;
			rect.y += 1f;

			property.isExpanded = EditorGUI.Foldout(rect, property.isExpanded, label);
			rect.y += 18f;

			if(property.isExpanded)
			{
				var prop = property.FindPropertyRelative("castType");
				DrawProperty(ref rect, prop);
				int type = prop.enumValueIndex;

				switch(type)
				{
					case 0: // Point
						DrawProperty(ref rect, property.FindPropertyRelative("offset"));
						break;
					case 1: // Sphere
						DrawProperty(ref rect, property.FindPropertyRelative("offset"));
						DrawProperty(ref rect, property.FindPropertyRelative("radius"));
						break;
					case 2: // Capsule
						DrawProperty(ref rect, property.FindPropertyRelative("offset"), point1Name);
						DrawProperty(ref rect, property.FindPropertyRelative("size"), point2Name);
						DrawProperty(ref rect, property.FindPropertyRelative("radius"));
						break;
					case 3: // Box
						DrawProperty(ref rect, property.FindPropertyRelative("offset"));
						DrawProperty(ref rect, property.FindPropertyRelative("size"), halfSizeName);
						DrawProperty(ref rect, property.FindPropertyRelative("orientation"));
						break;
				}

				if((prop = property.serializedObject.FindProperty("calcDirection")) != null && prop.boolValue)
					DrawProperty(ref rect, property.FindPropertyRelative("direction"), directionName);
			}

			EditorGUI.EndProperty();
		}

		private void DrawProperty(ref Rect rect, SerializedProperty prop, GUIContent label = null)
		{
			if(label == null)
				EditorGUI.PropertyField(rect, prop);
			else
				EditorGUI.PropertyField(rect, prop, label);
			rect.y += EditorGUI.GetPropertyHeight(prop) + 2f;
		}

		/// <summary>
		/// Property block with a choice of editable property.
		/// </summary>
		internal static void DrawEditorProperty(SerializedProperty property, GUIContent label, Editor editor, ref WeaponHitAreaInspector.EditProperty edit)
		{
			Rect rect = EditorGUILayout.GetControlRect(true);
			label = EditorGUI.BeginProperty(rect, label, property);
			property.isExpanded = EditorGUI.Foldout(rect, property.isExpanded, label);
			if(property.isExpanded)
			{
				EditorGUI.indentLevel++;
				var prop = property.FindPropertyRelative("castType");
				EditorGUI.BeginChangeCheck();
				EditorGUILayout.PropertyField(prop);
				int type = prop.enumValueIndex;
				if(EditorGUI.EndChangeCheck())
				{
					switch(type)
					{
						case 2:
							property.FindPropertyRelative("size").vector3Value = property.FindPropertyRelative("offset").vector3Value;
							break;
						case 3:
							property.FindPropertyRelative("size").vector3Value = new Vector3(0.5f, 0.5f, 0.5f);
							break;
					}
				}

				Vector3 center = (property.serializedObject.targetObject as Component).transform.TransformPoint(property.FindPropertyRelative("offset").vector3Value);

				switch(type)
				{
					case 0: // Point
						GUILayout.BeginHorizontal();
						EditorGUILayout.PropertyField(property.FindPropertyRelative("offset"));
						edit = EditToggle(edit, EditProperty.Offset, editor, center);
						GUILayout.EndHorizontal();
						break;

					case 1: // Sphere
						GUILayout.BeginHorizontal();
						EditorGUILayout.PropertyField(property.FindPropertyRelative("offset"));
						edit = EditToggle(edit, EditProperty.Offset, editor, center, property.FindPropertyRelative("radius").floatValue);
						GUILayout.EndHorizontal();
						GUILayout.BeginHorizontal();
						EditorGUILayout.PropertyField(property.FindPropertyRelative("radius"));
						edit = EditToggle(edit, EditProperty.Radius, editor, center, property.FindPropertyRelative("radius").floatValue);
						GUILayout.EndHorizontal();
						break;

					case 2: // Capsule
						Vector3 a = property.FindPropertyRelative("offset").vector3Value;
						Vector3 b = property.FindPropertyRelative("size").vector3Value;
						GUILayout.BeginHorizontal();
						EditorGUILayout.PropertyField(property.FindPropertyRelative("offset"), point1Name);
						edit = EditToggle(edit, EditProperty.Offset, editor, center, (a - b).magnitude / 2f + property.FindPropertyRelative("radius").floatValue);
						GUILayout.EndHorizontal();
						GUILayout.BeginHorizontal();
						EditorGUILayout.PropertyField(property.FindPropertyRelative("size"), point2Name);
						edit = EditToggle(edit, EditProperty.Offset2, editor, center, (a - b).magnitude / 2f + property.FindPropertyRelative("radius").floatValue);
						GUILayout.EndHorizontal();
						GUILayout.BeginHorizontal();
						EditorGUILayout.PropertyField(property.FindPropertyRelative("radius"));
						edit = EditToggle(edit, EditProperty.Radius, editor, center, property.FindPropertyRelative("radius").floatValue);
						GUILayout.EndHorizontal();
						break;

					case 3: // Box
						GUILayout.BeginHorizontal();
						EditorGUILayout.PropertyField(property.FindPropertyRelative("offset"));
						edit = EditToggle(edit, EditProperty.Offset, editor, center, property.FindPropertyRelative("size").vector3Value.magnitude);
						GUILayout.EndHorizontal();
						GUILayout.BeginHorizontal();
						EditorGUILayout.PropertyField(property.FindPropertyRelative("size"), halfSizeName);
						edit = EditToggle(edit, EditProperty.Size, editor, center, property.FindPropertyRelative("size").vector3Value.magnitude);
						GUILayout.EndHorizontal();
						GUILayout.BeginHorizontal();
						EditorGUILayout.PropertyField(property.FindPropertyRelative("orientation"));
						edit = EditToggle(edit, EditProperty.Rotation, editor, center, property.FindPropertyRelative("size").vector3Value.magnitude);
						GUILayout.EndHorizontal();
						break;
				}

				if((prop = property.serializedObject.FindProperty("calcDirection")) != null && prop.boolValue)
				{
					GUILayout.BeginHorizontal();
					EditorGUILayout.PropertyField(property.FindPropertyRelative("direction"), directionName);
					edit = EditToggle(edit, EditProperty.Direction, editor, center);
					GUILayout.EndHorizontal();
				}
				EditorGUI.indentLevel--;
			}
			EditorGUI.EndProperty();
		}

		/// <summary>
		/// Toggle to select a property to edit.
		/// </summary>
		private static EditProperty EditToggle(EditProperty value, EditProperty target, Editor editor, Vector3 center, float size = 1f)
		{
			EditorGUI.BeginChangeCheck();
			bool newValue = GUILayout.Toggle(value == target, editToggleContent, EditorStyles.miniButton, GUILayout.Width(32f));
			if(EditorGUI.EndChangeCheck())
			{
				if(newValue)
				{
					value = target;
				}
				else if(value == target)
				{
					value = EditProperty.None;
				}
				EditMode.ChangeEditMode(value != EditProperty.None ? EditMode.SceneViewEditMode.Collider : EditMode.SceneViewEditMode.None,
					new Bounds(center, Vector3.one * size), editor);
			}
			return value;
		}
	}
}