﻿using UnityEngine;
using UnityEditor;

namespace RAD.MeleeSystem
{
	[CustomPropertyDrawer(typeof(Resist.Pair))]
	public class ResistPairDrawer : PropertyDrawer
	{
		private const string TOOLTIP = "Max resist value - 1.0 (100% resist)";

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			label.tooltip = TOOLTIP;
			EditorGUI.BeginProperty(position, label, property);

			Rect rect = EditorGUI.PrefixLabel(position, label);

			int level = EditorGUI.indentLevel;//fix auto indent
			EditorGUI.indentLevel = 0;

			rect.width /= 2f;
			EnumProperty(rect, property.FindPropertyRelative("type"));
			rect.x += rect.width;
			FloatProperty(rect, property.FindPropertyRelative("_value"));

			EditorGUI.indentLevel = level;
			EditorGUI.EndProperty();
		}

		private static void EnumProperty(Rect position, SerializedProperty property)
		{
			EditorGUI.BeginChangeCheck();
			int enumValueIndex = EditorGUI.Popup(position, property.enumValueIndex, property.enumDisplayNames);
			if(EditorGUI.EndChangeCheck())
				property.enumValueIndex = enumValueIndex;
		}

		private static void FloatProperty(Rect position, SerializedProperty property)
		{
			EditorGUI.BeginChangeCheck();

			float floatValue = 0f;
			if(position.width >= 128f)
			{
				position.x += 4f;
				position.width -= 4f;
				floatValue = EditorGUI.Slider(position, property.floatValue, 0f, 1f);
			}
			else
			{
				floatValue = EditorGUI.FloatField(position, property.floatValue);

				if(floatValue < 0f)
					floatValue = 0f;
				else if(floatValue > 1f)
					floatValue = 1f;
			}

			if(EditorGUI.EndChangeCheck())
				property.floatValue = floatValue;
		}
	}
}