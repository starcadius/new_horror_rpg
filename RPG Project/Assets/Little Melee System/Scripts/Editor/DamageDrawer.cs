﻿using UnityEditor;
using UnityEngine;

namespace RAD.MeleeSystem
{
	[CustomPropertyDrawer(typeof(Damage))]
	public class DamageDrawer : PropertyDrawer
	{
		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			return EditorGUI.GetPropertyHeight(property.FindPropertyRelative("values"));
		}

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			EditorGUI.PropertyField(position, property.FindPropertyRelative("values"), label, true);
		}
	}
}