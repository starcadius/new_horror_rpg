﻿namespace RAD.MeleeSystem
{
	public enum DamageType
	{
		Instant,
		Slash,
		Piercing,
		Crush,
		Cutoff
	}
}