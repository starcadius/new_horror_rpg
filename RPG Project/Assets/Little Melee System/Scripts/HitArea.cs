﻿using UnityEngine;

namespace RAD.MeleeSystem
{
	/// <summary>
	/// Customizable raycasts area.
	/// </summary>
	[System.Serializable]
	public class HitArea
	{
		/// <summary>
		/// Type of cast
		/// </summary>
		public CastType castType;
		/// <summary>
		/// Local offset of area, or first point of capsule
		/// </summary>
		public Vector3 offset;
		[SerializeField]
		private Vector3 size;
		/// <summary>
		/// Half extents relative to the center of the box
		/// </summary>
		public Vector3 halfExtents { get { return size; } set { size = value; } }
		/// <summary>
		/// Second point of capsule
		/// </summary>
		public Vector3 offset2 { get { return size; } set { size = value; } }
		/// <summary>
		/// Local box cast orientation
		/// </summary>
		public Vector3 orientation;
		/// <summary>
		/// Radius of sphere or capsule
		/// </summary>
		public float radius;

		/// <summary>
		/// Blade direction
		/// </summary>
		public Vector3 direction;

		private Vector3 lastPosition;

		public void UpdatePosition(Transform transform)
		{
			lastPosition = transform.TransformPoint(offset);
		}

		/// <summary>
		/// Get all hits.
		/// Return false, if the area doesn't move or there are no hits.
		/// </summary>
		/// <param name="transform">Parent transform</param>
		/// <param name="calcDirection">Enable calculation of direction scalars</param>
		/// <param name="maxDeviation">The maximum deviation of the caste, at which the direction multiplier will be equal to one. Maybe from float.Epsilon to 1. 1 it's 180 degrees.</param>
		/// <returns>`True` - if have one or more hits, `False` - otherwise.</returns>
		public bool GetHits(Transform transform, bool calcDirection, float maxDeviation, LayerMask mask, out AreaData data, out RaycastHit[] hits)
		{
			data = GetAreaData(transform);

			hits = GetHits(transform, data.position, data.direction, data.distance, mask);

			if(hits == null || hits.Length < 1)
			{
				lastPosition = data.position;
				return false;
			}
			if(calcDirection)
			{
				if(maxDeviation < float.Epsilon)
					maxDeviation = float.Epsilon;
				if(maxDeviation > 1f)
					maxDeviation = 1f;

				Vector3 thisDirection = transform.TransformDirection(direction.normalized);
				data.scalarDirection = Vector3.Dot(data.direction, thisDirection);
				data.directionMultiplier = Mathf.Clamp01((data.scalarDirection - (1f - maxDeviation)) / maxDeviation);
			}
			else
				data.directionMultiplier = 1f;

			return true;
		}

		public AreaData GetAreaData(Transform transform)
		{
			Vector3 position = transform.TransformPoint(offset);
			Vector3 vector = position - lastPosition;

			AreaData data = new AreaData();
			data.deltaTime = Time.deltaTime;
			data.position = position;
			data.lastPosition = lastPosition;
			data.distance = vector.magnitude;
			data.direction = vector.normalized;
			return data;
		}

		private RaycastHit[] GetHits(Transform transform, Vector3 position, Vector3 direction, float distance, LayerMask layer)
		{
			switch(castType)
			{
				case CastType.Point:
					return Physics.RaycastAll(position, direction, distance, layer, QueryTriggerInteraction.UseGlobal);
				case CastType.Sphere:
					return Physics.SphereCastAll(position, radius, direction, distance, layer, QueryTriggerInteraction.UseGlobal);
				case CastType.Capsule:
					return Physics.CapsuleCastAll(position, transform.TransformPoint(size), radius, direction, distance, layer, QueryTriggerInteraction.UseGlobal);
				case CastType.Box:
					return Physics.BoxCastAll(position, size, direction, transform.rotation * Quaternion.Euler(orientation), distance, layer, QueryTriggerInteraction.UseGlobal);
			}
			return null;
		}

#if UNITY_EDITOR
		private static readonly Color gizmoColor = new Color(1f, 0.75f, 0.75f);
		private static readonly Color arrowColor = new Color(1f, 1f, 0.75f);

		internal bool drawDirection;
		internal Transform transform;
		[System.NonSerialized]
		public bool draw = true; // Inspector temporarily disables drawing when editing properties.

		public void DrawGizmos()
		{
			if(!draw) return;
			UnityEditor.Handles.color = gizmoColor;
			Gizmos.color = gizmoColor;
			switch(castType)
			{
				case CastType.Point:
					DrawRay();
					break;
				case CastType.Sphere:
					DrawSphereArea();
					break;
				case CastType.Capsule:
					DrawCapsuleArea();
					break;
				case CastType.Box:
					DrawBoxArea();
					break;
			}
		}

		void DrawRay()
		{
			Vector3 point = transform.TransformPoint(offset);
			UnityEditor.Handles.DrawSolidDisc(point,
				UnityEditor.SceneView.lastActiveSceneView != null ? (UnityEditor.SceneView.lastActiveSceneView.camera.transform.position - point).normalized : Vector3.up,
				UnityEditor.HandleUtility.GetHandleSize(point) * 0.05f);

			if(drawDirection)
				DrawArrow(point, transform.TransformDirection(direction.normalized), 0.25f);
		}

		void DrawCapsuleArea()
		{
			Vector3
				point1 = transform.TransformPoint(offset),
				point2 = transform.TransformPoint(size),
				normal = Vector3.Normalize(point2 - point1);

			Quaternion rotation = Quaternion.FromToRotation(Vector3.up, normal);
			Vector3 forward = rotation * Vector3.forward * radius, right = rotation * Vector3.right * radius, up = normal * radius;

			UnityEditor.Handles.DrawWireArc(point1, up, right, 360f, radius);
			UnityEditor.Handles.DrawWireArc(point1, right, forward, 180f, radius);
			UnityEditor.Handles.DrawWireArc(point1, forward, right, -180f, radius);

			UnityEditor.Handles.DrawWireArc(point2, up, right, 360f, radius);
			UnityEditor.Handles.DrawWireArc(point2, right, forward, -180f, radius);
			UnityEditor.Handles.DrawWireArc(point2, forward, right, 180f, radius);

			UnityEditor.Handles.DrawLine(point1 - forward, point2 - forward);
			UnityEditor.Handles.DrawLine(point1 + forward, point2 + forward);
			UnityEditor.Handles.DrawLine(point1 - right, point2 - right);
			UnityEditor.Handles.DrawLine(point1 + right, point2 + right);

			if(drawDirection)
				DrawArrow((point1 + point2) * 0.5f, transform.TransformDirection(direction.normalized), 0.25f);
		}

		void DrawSphereArea()
		{
			Vector3 point = transform.TransformPoint(offset);

			Gizmos.DrawWireSphere(point, radius);

			if(drawDirection)
				DrawArrow(point, transform.TransformDirection(direction.normalized), 0.25f);
		}

		void DrawBoxArea()
		{
			Vector3 center = transform.TransformPoint(offset);
			Quaternion rotation = transform.rotation * Quaternion.Euler(orientation);
			Vector3
				halfSize = size * 2f,
				right = rotation * new Vector3(halfSize.x, 0f, 0f),
				up = rotation * new Vector3(0f, halfSize.y, 0f),
				forward = rotation * new Vector3(0f, 0f, halfSize.z);

			Vector3[] vertices = new Vector3[8];
			vertices[0] = center + up + forward + right;
			vertices[1] = center + up + forward - right;
			vertices[2] = center + up - forward - right;
			vertices[3] = center + up - forward + right;

			vertices[4] = center - up + forward + right;
			vertices[5] = center - up + forward - right;
			vertices[6] = center - up - forward - right;
			vertices[7] = center - up - forward + right;

			UnityEditor.Handles.DrawLine(vertices[0], vertices[1]);
			UnityEditor.Handles.DrawLine(vertices[1], vertices[2]);
			UnityEditor.Handles.DrawLine(vertices[2], vertices[3]);
			UnityEditor.Handles.DrawLine(vertices[3], vertices[0]);

			UnityEditor.Handles.DrawLine(vertices[4], vertices[5]);
			UnityEditor.Handles.DrawLine(vertices[5], vertices[6]);
			UnityEditor.Handles.DrawLine(vertices[6], vertices[7]);
			UnityEditor.Handles.DrawLine(vertices[7], vertices[4]);

			UnityEditor.Handles.DrawLine(vertices[0], vertices[4]);
			UnityEditor.Handles.DrawLine(vertices[1], vertices[5]);
			UnityEditor.Handles.DrawLine(vertices[2], vertices[6]);
			UnityEditor.Handles.DrawLine(vertices[3], vertices[7]);

			if(drawDirection)
				DrawArrow(center, transform.TransformDirection(direction.normalized), 0.25f);
		}

		void DrawArrow(Vector3 center, Vector3 dir, float raduis)
		{
			UnityEditor.Handles.color = arrowColor;
			raduis *= UnityEditor.HandleUtility.GetHandleSize(center);
			Quaternion rotation = Quaternion.FromToRotation(Vector3.up, dir);
			Vector3 forward = rotation * Vector3.forward * raduis, right = rotation * Vector3.right * raduis;
			Vector3 from = center - dir * raduis;
			Vector3 to = center + dir * raduis;

			UnityEditor.Handles.DrawWireArc(from, dir, right, 360f, raduis);

			UnityEditor.Handles.DrawLine(to, from + forward);
			UnityEditor.Handles.DrawLine(to, from - forward);
			UnityEditor.Handles.DrawLine(to, from + right);
			UnityEditor.Handles.DrawLine(to, from - right);
		}
#endif
	}

	public enum CastType
	{
		Point = 0,
		Sphere = 1,
		Capsule = 2,
		Box = 3
	}

	public struct AreaData
	{
		public float deltaTime;
		//Some coefficient based on the motion vector and the direction of cast, taking into account deviation
		public float directionMultiplier;
		//Dot product of hit direction and area move direction
		public float scalarDirection;
		//Move direction
		public Vector3 direction;
		//Previous frame area position
		public Vector3 lastPosition;
		//Current area position
		public Vector3 position;
		//Max distance of cast
		public float distance;
	}
}