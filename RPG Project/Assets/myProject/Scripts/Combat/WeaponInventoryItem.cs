﻿using RPG.Core;
using UnityEngine;
using RPG.Resources;
using MoreMountains.InventoryEngine;
using MoreMountains.Tools;
namespace RPG.Combat
{
	[CreateAssetMenu(fileName = "WeaponInventoryitem", menuName = "Weapon/Make New Inventory Weapon", order = 0)]
	public class WeaponInventoryItem : InventoryItem
	{
		[Header("Weapon")]
		/// the sprite to use to show the weapon when equipped
		public Sprite WeaponSprite;

		/// <summary>
		/// What happens when the object is used 
		/// </summary>
		public override bool Equip()
		{
			base.Equip();
			InventoryDemoGameManager.Instance.Player.SetWeapon(this);
			return true;
		}

		/// <summary>
		/// What happens when the object is used 
		/// </summary>
		public override bool UnEquip()
		{
			base.UnEquip();
			InventoryDemoGameManager.Instance.Player.SetWeapon(this);
			return true;
		}

		 public override void SpawnPrefab()
		{
			if (TargetInventory != null)
			{
				// if there's a prefab set for the item at this slot, we instantiate it at the specified offset
				if (Prefab != null && TargetInventory.TargetTransform != null)
				{
					GameObject droppedObject = (GameObject)Instantiate(Prefab);
					if (droppedObject.GetComponent<ItemPicker>() != null)
					{
						droppedObject.GetComponent<ItemPicker>().Quantity = Quantity;
					}
					// we randomize the drop position
					Vector3 randomDropDirection = UnityEngine.Random.insideUnitSphere;
					randomDropDirection.Normalize();
					Vector3 randomDropDistance = MMMaths.RandomVector3(PrefabDropMinDistance, PrefabDropMaxDistance);
					randomDropDirection = Vector3.Scale(randomDropDirection, randomDropDistance);

					if (SpawnShape == SpawnShapes.HalfCircle)
					{
						randomDropDirection.y = Mathf.Abs(randomDropDirection.y);
					}

					droppedObject.transform.position = InventoryDemoGameManager.Instance.Player.transform.position + randomDropDirection;
				}
			}
		}

		//================================================================================================================



		[SerializeField] AnimatorOverrideController animatorOverride = null;
		[SerializeField] GameObject equippedPrefab = null;
		[SerializeField] float weaponDamage = 5f;
		[SerializeField] float percentageBonus = 0;
		[SerializeField] float weaponRange = 2f;
		[SerializeField] bool isRightHanded = true;
		[SerializeField] Projectile projectile = null;

		const string weaponName = "Weapon";

		//================================================



		//public GameObject inHandWeapon;



		//[SerializeField] float timeBetweenAttacks = 1f;


		//[SerializeField] ProjectileRigidbody projectileRB = null;
		//Transform projectilelaunchLocation = null;
		//[SerializeField] int ammoAmount = 0;
		//[SerializeField] AudioClip soundEffect = null;
		//[SerializeField] GameObject meleeHitEffect = null;


		public void Spawn(Transform rightHand, Transform leftHand, Animator animator)
		{
			DestroyOldWeapon(rightHand, leftHand);

			if (equippedPrefab != null)
			{
				Transform handTransform = GetTransform(rightHand, leftHand);
				GameObject weapon = Instantiate(equippedPrefab, handTransform);
				weapon.name = weaponName;
			}

			var overrideController = animator.runtimeAnimatorController as AnimatorOverrideController;
			if (animatorOverride != null)
			{
				animator.runtimeAnimatorController = animatorOverride;
			}
			else if (overrideController != null)
			{
				animator.runtimeAnimatorController = overrideController.runtimeAnimatorController;
			}
		}

		//public void Spawn(Transform rightHandTransform, Transform leftHandTransform, Animator animator)
		//{
		//	if (equippedPrefab != null)
		//	{
		//		Transform handTransform = GetTransform(rightHandTransform, leftHandTransform);

		//		inHandWeapon = Instantiate(equippedPrefab, handTransform);
		//		Debug.Log("inHandWeapon = "+ inHandWeapon.gameObject.name);
		//		if (projectile != null || projectileRB != null)
		//		{
		//			projectilelaunchLocation = inHandWeapon.GetComponentInChildren<LaunchLocation>().transform;
		//		}

		//	}

		//	var overrideController = animator.runtimeAnimatorController as AnimatorOverrideController;

		//	if (animatorOverride != null)
		//	{
		//		animator.runtimeAnimatorController = animatorOverride;
		//	}
		//	else if(overrideController != null)
		//	{
		//		animator.runtimeAnimatorController = overrideController.runtimeAnimatorController;
		//	}
		//}

		private void DestroyOldWeapon(Transform rightHand, Transform leftHand)
		{
			Transform oldWeapon = rightHand.Find(weaponName);
			if (oldWeapon == null)
			{
				oldWeapon = leftHand.Find(weaponName);
			}
			if (oldWeapon == null) return;

			oldWeapon.name = "DESTROYING";
			Destroy(oldWeapon.gameObject);
		}

		private Transform GetTransform(Transform rightHand, Transform leftHand)
		{
			Transform handTransform;
			if (isRightHanded) handTransform = rightHand;
			else handTransform = leftHand;
			return handTransform;
		}

		public bool HasProjectile()
		{
			return projectile != null;
		}

		//public bool IsProjectile()
		//{
		//	return projectile != null;
		//}

		public void LaunchProjectile(Transform rightHand, Transform leftHand, Health target, GameObject instigator, float calculatedDamage)
		{
			Projectile projectileInstance = Instantiate(projectile, GetTransform(rightHand, leftHand).position, Quaternion.identity);
			projectileInstance.SetTarget(target, instigator, calculatedDamage);
		}

		/*
		public void LaunchProjectile(Transform rightHand, Transform leftHand, Transform target, Health targetHealth = null, string shooterTag = null, GameObject instigator = null)
		{

			Transform rotationTarget = GetTransform(rightHand, leftHand);

			Transform aimTarget = target;
			if (projectileRB != null)
			{
				ProjectileRigidbody projectileInstance = Instantiate(projectileRB, GetTransform(rightHand, leftHand).position, rotationTarget.rotation);
				projectileInstance.SetShooterTag(shooterTag);
				projectileInstance.transform.LookAt(aimTarget);

				projectileInstance.SetTarget(target, weaponDamage, instigator );
				projectileInstance = null;


			}
			else
			{
				Projectile projectileInstance = Instantiate(projectile, GetTransform(rightHand, leftHand).position, rotationTarget.rotation);
				if (projectilelaunchLocation != null)
				{
					projectileInstance.transform.position = projectilelaunchLocation.position;
				}
				projectileInstance.SetShooterTag(shooterTag);
				projectileInstance.SetTarget(target, weaponDamage, instigator);
				aimTarget.position = projectileInstance.GetAimLocation();
				projectileInstance.transform.LookAt(aimTarget);
				projectileInstance = null;
			}

		}
		*/

		public float GetDamage()
		{
			return weaponDamage;
		}

		public float GetPercentageBonus()
		{
			return percentageBonus;
		}

		public float GetRange()
		{
			return weaponRange;
		}

		//public float GetTimeBetweenAttacks()
		//{
		//	return timeBetweenAttacks;
		//}

		//public AudioClip GetSoundEffect()
		//{
		//	return soundEffect;
		//}

		//public GameObject GetMeleeHitEffect()
		//{
		//	return meleeHitEffect;
		//}

		//public void DoHitEffect(Transform hitLocation)
		//{
		//	GameObject hitEffect = Instantiate(meleeHitEffect, hitLocation.position, hitLocation.rotation);
		//}


	}
}
