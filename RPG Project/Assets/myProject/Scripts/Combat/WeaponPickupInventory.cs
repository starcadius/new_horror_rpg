﻿using UnityEngine;
using System.Collections;
using MoreMountains.InventoryEngine;
using RPG.Control;
using System;

namespace RPG.Combat
{
	public class WeaponPickupInventory : ItemPicker, IRaycastable
	{
		//[SerializeField] Weapon weapon = null;
		[SerializeField] AudioSource audioSource;
		[SerializeField] AudioClip soundEffect = null;
		[SerializeField] string soundEffectGroup = "";//for audioSourcepro
		// Use this for initialization
		void Awake()
		{
			audioSource = GetComponent<AudioSource>();
		}

		/// <summary>
		/// Describes what happens when the object is successfully picked
		/// </summary>
		 protected override void PickSuccess()
		{
			print("Got "+ Item);
			if (soundEffect != null)
			{
				audioSource.clip = soundEffect;
				//audioSource.PlayOneShot(audioSource.clip, Random.Range(1, 0.75f));
				if (soundEffectGroup != "")
				{
					AudioSourcePro.PlayClipAtPoint(SoundManager.LoadFromGroup(soundEffectGroup), this.transform.position);
				}
				else if (soundEffect != null)
				{
					AudioSourcePro.PlayClipAtPoint(soundEffect, this.transform.position);
				}
			}
		}

		public bool HandleRaycast(PlayerController callingController)
		{
			if (Input.GetMouseButtonDown(0))
			{
				Pickup(callingController.GetComponent<Fighter>());
			}
			return true;
		}

		private void Pickup(Fighter fighter)
		{

			Debug.Log("pickup");
			Pick(Item.TargetInventoryName);
		}

		public CursorType GetCursorType()
		{
			return CursorType.Pickup;
		}

		//override public void OnTriggerEnter(Collider collider)
		//{
		//	//if what's colliding with the picker ain't a characterBehavior, we do nothing and exit
		//	if (collider.tag != "Player")
		//	{
		//		return;
		//	}
		//	Pick(Item.TargetInventoryName);
		//	//collider.gameObject.GetComponent<Fighter>().TakeWeapon(weapon);

		//	if (soundEffect != null)
		//	{
		//		audioSource.clip = soundEffect;
		//		audioSource.PlayOneShot(audioSource.clip, Random.Range(1, 0.75f));
		//		Debug.Log("played equip sound " + soundEffect.name + " on gameObject " + gameObject.name);
		//		foreach (Transform child in this.transform)
		//		{
		//			child.gameObject.SetActive(false);
		//		}
		//		Destroy(gameObject, audioSource.clip.length);
		//	}
		//	else
		//	{
		//		Destroy(gameObject);
		//	}

		//}
	}
}
