﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;
using MoreMountains.InventoryEngine;
namespace RPG.PlayerInventory
{
	public class PlayerInventory : MonoBehaviour, MMEventListener<MMInventoryEvent>
	{
		/// the sprite used to show the current weapon
		public SpriteRenderer WeaponSprite;
		/// the armor inventory
		public Inventory ArmorInventory;
		[SerializeField] string armorInventoryName = "RogueArmorInventory";
		/// the weapon inventory
		public Inventory WeaponInventory;
		[SerializeField] string weaponInventoryName = "RogueWeaponInventory";
		/// the books inventory
		public Inventory BooksInventory;
		[SerializeField] string booksInventoryName = "booksInventory";

		protected int _currentArmor = 0;
		protected int _currentWeapon = 0;

		/// <summary>
		/// Sets the current armor.
		/// </summary>
		/// <param name="index">Index.</param>
		public virtual void SetArmor(int index)
		{
			_currentArmor = index;
		}

		/// <summary>
		/// Sets the current weapon sprite
		/// </summary>
		/// <param name="newSprite">New sprite.</param>
		/// <param name="item">Item.</param>
		public virtual void SetWeapon(InventoryItem item)
		{
			//could add 3d weapon here
			
			//WeaponSprite.sprite = newSprite;
		}


		/// <summary>
		/// Catches MMInventoryEvents and if it's an "inventory loaded" one, equips the first armor and weapon stored in the corresponding inventories
		/// </summary>
		/// <param name="inventoryEvent">Inventory event.</param>
		public virtual void OnMMEvent(MMInventoryEvent inventoryEvent)
		{
			if (inventoryEvent.InventoryEventType == MMInventoryEventType.InventoryLoaded)
			{
				if (inventoryEvent.TargetInventoryName == armorInventoryName)
				{
					if (ArmorInventory != null)
					{
						if (!InventoryItem.IsNull(ArmorInventory.Content[0]))
						{
							ArmorInventory.Content[0].Equip();
						}
					}
				}
				if (inventoryEvent.TargetInventoryName == weaponInventoryName)
				{
					if (WeaponInventory != null)
					{
						if (!InventoryItem.IsNull(WeaponInventory.Content[0]))
						{
							WeaponInventory.Content[0].Equip();
						}
					}
				}

				if (inventoryEvent.TargetInventoryName == booksInventoryName)
				{
					if (BooksInventory != null)
					{
						if (!InventoryItem.IsNull(BooksInventory.Content[0]))
						{
							BooksInventory.Content[0].Equip();
						}
					}
				}
			}
		}

		/// <summary>
		/// On Enable, we start listening to MMInventoryEvents
		/// </summary>
		protected virtual void OnEnable()
		{
			this.MMEventStartListening<MMInventoryEvent>();
		}


		/// <summary>
		/// On Disable, we stop listening to MMInventoryEvents
		/// </summary>
		protected virtual void OnDisable()
		{
			this.MMEventStopListening<MMInventoryEvent>();
		}
	}
}
